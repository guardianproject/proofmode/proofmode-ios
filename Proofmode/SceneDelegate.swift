//
//  SceneDelegate.swift
//  Proofmode
//
//  Created by N-Pex on 2020-07-15.
//

import UIKit
import SwiftUI
import LibProofMode
import CoreData

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    var settings = Settings.shared
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
        // Create the SwiftUI view that provides the window contents.
        let contentView = ContentView().environmentObject(settings).environment(\.managedObjectContext, Activities.shared.container.viewContext)
        
        // Use a UIHostingController as window root view controller.
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = UIHostingController(rootView: contentView
                .environmentObject(settings)
                .environmentObject(MediaItemCollection(mediaItems:[]))
            )
            self.window = window
            if #available(iOS 13.0, *) {
                window.overrideUserInterfaceStyle = .light
            }
            window.makeKeyAndVisible()
        }
        
        // Call this here to make sure CLLocationManager is created on the main thread!
        let _ = LocationManager.shared.cll
        
        //let _ = MediaWatcher.shared // Touch this to start listening
        let proof = Proof.shared
        proof.defaultDocumentFolder = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.org.witness.proofmode.ios")
        ?? FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        if Activities.shared.loaded {
            Activities.shared.updateActivitiesGroupedForDisplay()
            Activities.shared.updateAllCapturedAndImportedItems()
            Activities.shared.lastUpdated = Date.now
        }
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    
}
