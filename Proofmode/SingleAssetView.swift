//
//  SingleAssetView.swift
//  Proofmode
//
//  Created by N-Pex on 2023-03-20.
//

import SwiftUI
import Photos
import CoreLocation
import LibProofMode
import AVKit

private let metadataIconSize = CGFloat(27)

struct SingleAssetView_Previews: PreviewProvider {
    static var previews: some View {
        let settings = Settings.shared
        settings.onboarded = true
        settings.isOn = true
        return Group {
            SingleAssetView(vm: SingleAssetViewViewModel())
                .previewLayout(.sizeThatFits)
        }
    }
}
class SingleAssetViewViewModel: ObservableObject {
    @Published var allAssets: [CameraItem] = [] {
        didSet {
            self.objectWillChange.send()
        }
    }
    @Published var selectedIndex: Int = -1 {
        didSet {
            self.objectWillChange.send()
        }
    }
    @Published var selectedIndexAnimated: CGFloat = CGFloat(-1) {
        didSet {
            self.objectWillChange.send()
        }
    }
}

struct SingleAssetView: View {
    @Environment(\.isPreview) var isPreview
    @Environment(\.onBundleItems) var onBundleItems
    @Environment(\.onC2PAShareItems) var onC2PAShareItems
    
    @ObservedObject var vm: SingleAssetViewViewModel
    @StateObject private var videoPlayerViewModel: VideoPlayerViewModel = VideoPlayerViewModel()
    
    @State var selectedAssets: [CameraItem] = []
    @State var selectedAsset: CameraItem? = nil
    @State var selectionMode: Bool = false
    @State var itemSizeMultiplier: CGFloat = 1.0
    @State var showMetadata: Bool = false
    @State var metadataOpacity: CGFloat = 0
    @State var showShareOptions: Bool = false
    @State var showC2PAWarning: Bool = false
    
    public func update(assets: [CameraItem], index: Int) {
        self.vm.allAssets = assets
        self.vm.selectedIndex = index
        self.vm.selectedIndexAnimated = CGFloat(index)
    }
    
    var body: some View {
        GeometryReader { geo in
            if geo.size.width > 0, geo.size.height > 0 {
                let topHeight = geo.size.height - 64
                VStack() {
                    ZStack(alignment: .topLeading) {
                        itemView()
                            .frame(height: topHeight / 2 + (1 - metadataOpacity) * ((topHeight / 2 - 74)))
                            .if (!videoPlayerViewModel.isPlaying()) { view in
                                // Swipe up/down to enter/leave metadata mode
                                    view.modifier(DragGestureViewModifier(
                                        highPriority: true,
                                        onUpdate: { val in
                                            metadataOpacity = max(min(1.0, (showMetadata ? 1 : 0) - (val.translation.height / (geo.size.height / 3))), 0)
                                        },
                                        onEnd: { maybeVal in
                                            withAnimation(.spring()) {
                                                if metadataOpacity > 0.2 && !showMetadata {
                                                    showMetadata = true
                                                } else if metadataOpacity < 0.8 && showMetadata {
                                                    showMetadata = false
                                                }
                                                metadataOpacity = showMetadata ? 1 : 0
                                            }
                                        }))
                            }

                        VStack {
                            Spacer()
                            AssetMetadataView(asset: $selectedAsset)
                                .opacity(metadataOpacity)
                                .frame(width: geo.size.width, height: topHeight / 2)
                        }
                        
                        VStack {
                            Spacer()
                            previewsView(geo: geo)
                                .offset(x: 0, y: metadataOpacity * 64)
                                .opacity(1 - metadataOpacity)
                                .frame(height: 64)
                        }
                        
                        if selectedAssets.count > 0 {
                            selectionHeaderView()
                        }
                    }.frame(height: topHeight)
                    
                    HStack {
                        Spacer()
                        Button(action: {
                            // For now, remove any options, always share the zip.
                            //self.showShareOptions = true
                            var assets = selectedAssets
                            if selectedAssets.count == 0, let selectedAsset = selectedAsset {
                                assets = [selectedAsset]
                            }
                            if assets.count > 0 {
                                self.onBundleItems?(assets)
                            }
                        }) {
                            Image(systemName: "square.and.arrow.up").resizable().scaledToFit().frame(width: 24, height: 24)
                        }
                        Spacer()
                    }
                    .padding(20)
                    .frame(height: 64)
                }
                .frame(width: geo.size.width, height: geo.size.height)
                .onChange(of: selectionMode) { val in
                    withAnimation {
                        itemSizeMultiplier = val ? 0.7 : 1.0
                    }
                }
                .onChange(of: selectedAssets) { val in
                    selectionMode = val.count > 0
                }
                .task(id: vm.selectedIndex, {
                    if vm.selectedIndex >= 0 && vm.selectedIndex < vm.allAssets.count {
                        selectedAsset = vm.allAssets[vm.selectedIndex]
                    } else {
                        selectedAsset = nil
                    }
                    videoPlayerViewModel.loadAsset(selectedAsset)
                })
                .confirmationDialog("", isPresented: $showShareOptions, titleVisibility: .hidden) {
                    Button("Share Proof Zip") {
                        withAnimation {
                            if selectedAssets.count == 0, let selectedAsset = selectedAsset {
                                selectedAssets = [selectedAsset]
                            }
                            if selectedAssets.count > 0 {
                                self.onBundleItems?(selectedAssets)
                            }
                        }
                    }
                    
                    Button("Add Content Credentials") {
                        self.showC2PAWarning = true
                    }
                }
                .confirmationDialog("Share with ProofSign cloud service to add Content Credentials (C2PA) to this photo?", isPresented: $showC2PAWarning, titleVisibility: .visible) {
                    Button("OK") {
                        withAnimation {
                            if selectedAssets.count == 0, let selectedAsset = selectedAsset {
                                selectedAssets = [selectedAsset]
                            }
                            if selectedAssets.count > 0 {
                                self.onC2PAShareItems?(selectedAssets)
                            }
                        }
                    }
                    
                    Button("Cancel", role: .cancel) {
                    }
                }
            }
        }
        .navigationTitle(selectedAsset?.creationDate?.formatted(.dateTime.day().month().year()) ?? "")
        .navigationBarItems(trailing: navigationButtonRight())
        .navigationBarTitleDisplayMode(.automatic)
        .onReceive(NotificationCenter.default.publisher(for: Notification.Name.selectionClearedNotification)) { _ in
            selectedAssets = []
          }
    }
    
    private func itemId(_ index: Int) -> String {
        if index >= 0, index < self.vm.allAssets.count {
            return "list_item_\(self.vm.allAssets[index].id)"
        }
        return "\(index)"
    }
    
    private func selectionHeaderView() -> some View {
        return GeometryReader { geo in
            VStack {
                HStack(alignment: .top,spacing: 10) {
                    if let item = self.selectedAssets.first {
                        AssetView(asset: item, cornerRadius: 4)
                            .frame(width: 54, height: 54)
                    }
                    Text("\(selectedAssets.count) Items Selected")
                    Spacer()
                    Button(action: {
                        selectedAssets = []
                    }) {
                        Image("ic_dismiss")
                            .resizable()
                            .tint(nil)
                            .frame(width: 30, height: 30)
                    }
                }
                .padding(8)
                
                Divider()
            }
            .frame(maxWidth: .infinity)
        }
    }
    
    private func itemView() -> some View {
        return GeometryReader { geo in
            let itemWidth: CGFloat = itemSizeMultiplier * geo.size.width
            let itemWidthPlusMargin: CGFloat = itemWidth + 20.0
            let numItems: Int = 5
            let idxSelected: Int = Int((numItems - 1) / 2)
            let items: [(Int, Int, String)] = Array(0..<numItems).map { e in
                return (e, e + vm.selectedIndex - idxSelected, itemId(e + vm.selectedIndex - idxSelected))
            }
            let selectedItemOffset: CGFloat = (CGFloat(idxSelected) * itemWidthPlusMargin) - (geo.size.width - itemWidth) / 2
            let partialItem: CGFloat = CGFloat(vm.selectedIndex) - vm.selectedIndexAnimated
            let offsetX: CGFloat = itemWidthPlusMargin * partialItem - selectedItemOffset
            
            ZStack {
                HStack(alignment: .top, spacing: 20) {
                    ForEach(items, id: \.2) { tuple in
                        let delta = idxSelected - tuple.0
                        let idxThis = vm.selectedIndex - delta
                        if idxThis >= 0 && idxThis < vm.allAssets.count {
                            let item = vm.allAssets[idxThis]
                            
                            let longPress = LongPressGesture().onEnded { _ in
                                if selectedAssets.count == 0 {
                                    selectedAssets.append(item)
                                }
                            }
                            let tap = TapGesture().onEnded { _ in
                                if let index = selectedAssets.firstIndex(of: item) {
                                    selectedAssets.remove(at: index)
                                } else if selectedAssets.count > 0 {
                                    selectedAssets.append(item)
                                }
                            }
                            
                            AssetView(asset: item, corners: [], contain: true, selectionVisualization: .checkmark)
                                .frame(width: itemWidth)
                                .id(item.id)
                                .highPriorityGesture(tap.simultaneously(with: longPress))
                                .environment(\.selectedAssets, self.selectedAssets)
                        } else {
                            // An empty placeholder to avoid special offset calculations etc
                            //
                            Color.clear
                                .frame(width: itemWidth,height: min(geo.size.width, geo.size.height))
                        }
                    }
                }
                .offset(x: offsetX)
                .modifier(DragGestureViewModifier(
                    highPriority: false,
                    onUpdate: { val in
                        let offset: CGFloat = val.location.x - val.startLocation.x
                        let partialIndex: CGFloat = offset / itemWidthPlusMargin
                        vm.selectedIndexAnimated = CGFloat(vm.selectedIndex) - partialIndex
                    },
                    onEnd: { maybeVal in
                        if let val = maybeVal {
                            let offset: CGFloat = val.location.x - val.startLocation.x
                            let partialIndex: CGFloat = offset / itemWidthPlusMargin
                            vm.selectedIndexAnimated = CGFloat(vm.selectedIndex) - partialIndex
                            
                            if offset > 50 && vm.selectedIndex > 0 {
                                vm.selectedIndex = vm.selectedIndex - 1
                            } else if offset < -50 && vm.selectedIndex < (vm.allAssets.count - 1) {
                                vm.selectedIndex = vm.selectedIndex + 1
                            }
                            withAnimation(.spring()) {
                                vm.selectedIndexAnimated = CGFloat(vm.selectedIndex)
                            }
                        }
                    }))
                .highPriorityGesture(TapGesture(), including: .subviews)
                
                if partialItem == 0 {
                    ZStack {
                        if videoPlayerViewModel.showPlayer, let player = videoPlayerViewModel.avPlayer {
                            VideoPlayer(player: player)
                                .frame(width: itemWidth)
                                .clipped()
                                .onAppear() {
                                    player.play()
                                }
                        } else if selectedAsset?.asset?.mediaType == .video {
                            Button(action: {
                                videoPlayerViewModel.play()
                            }) {
                                Image(systemName: "play.circle.fill")
                                    .resizable()
                                    .frame(width: itemWidth / 10, height: itemWidth / 10)
                                    .tint(.white)
                            }
                        }
                        
                    }
                    .offset(x: offsetX)
                }
            }
        }
    }
    
    private func previewsView(geo: GeometryProxy) -> some View {
        let selectedIndex = vm.selectedIndex
        return ScrollViewReader { scrollView in
            ScrollView(.horizontal, showsIndicators: false) {
                LazyHStack(spacing: 0) {
                    ForEach(Array(vm.allAssets.enumerated()), id: \.element.id) { index, item in
                        HStack(spacing: 0) {
                            if index == selectedIndex {
                                Color.white.frame(width: 8)
                            }
                            AssetView(asset: item, corners: [])
                                .frame(width: 40, height: 64)
                                .id(item.id)
                                .highPriorityGesture(TapGesture()
                                    .onEnded {
                                        self.vm.selectedIndex = index
                                        self.vm.selectedIndexAnimated = CGFloat(index)
                                    }
                                )
                            if index == selectedIndex {
                                Color.white.frame(width: 8)
                            }
                        }
                    }
                }
            }
            .onChange(of: selectedIndex) { newValue in
                // Scroll to current item
                if newValue >= 0 && newValue < vm.allAssets.count {
                    scrollView.scrollTo(vm.allAssets[newValue].id, anchor: .center)
                }
            }
            .onAppear {
                if selectedIndex >= 0 && selectedIndex < vm.allAssets.count {
                    scrollView.scrollTo(vm.allAssets[selectedIndex].id, anchor: .center)
                }
            }
        }
    }
    
    private func navigationButtonRight() -> some View {
        return Button(action: {
            withAnimation {
                showMetadata = !showMetadata
                metadataOpacity = showMetadata ? 1.0 : 0.0
            }
        }) {
            Image(systemName: showMetadata ? "info.circle.fill" : "info.circle")
        }
    }
}

struct MetadataEntry: Identifiable, Equatable {
    public var id: String
    public var value: String
}
    
struct AssetMetadataView: View {
    @Binding var asset: CameraItem?
    @State var proofData: [String: String]?
    @State var locationDisplayName: String = ""
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: true) {
            Group {
                VStack(alignment: .leading) {
                    displayDate()
                    metaLocation()
                    metaDevice()
                    metaConnection()
                    
                    if let data: [MetadataEntry] = proofData?.map({ MetadataEntry(id: $0.key, value: $0.value ) }).sorted(by: { (left: MetadataEntry, right: MetadataEntry) in
                        return left.id < right.id
                    }) {
                        ForEach(data) { entry in
                            VStack(alignment: .leading, spacing: 2) {
                                Text(entry.id)
                                    .font(Font.custom(Font.titleFont, size: 13).smallCaps())
                                    .fontWeight(.bold)
                                    .foregroundColor(.black)
                                Text(entry.value)
                                    .font(Font.custom(Font.bodyFont, size: 15))
                                    .fontWeight(.regular)
                                    .multilineTextAlignment(.leading)
                                    .foregroundColor(.black)
                                    .padding([.bottom], 4)
                            }.padding([.top], 10)
                        }
                    }
                }
                .frame(maxWidth: .infinity, alignment: .leading)
            }
        }
        .padding(16)
        .task(id: asset) {
            update()
        }
    }
    
    private func displayDate() -> some View {
        return HStack {
            if let asset = asset, let date = asset.creationDate {
                Text(date.metadataDateTime())
                Text(date.metadataDateDate()).fontWeight(.bold)
            }
        }
    }
    
    private func metaLocation() -> AnyView {
        if let proofData = proofData,
           let latVal = proofData["Location.Latitude"],
           let lat = Float(latVal),
           let lonVal = proofData["Location.Longitude"],
           let lon = Float(lonVal)
        {
            return metadataView("ic_location", title: "", text: "\(String(format:"%.1f", lat)) lat, \(String(format:"%.1f", lon)) long")
        } else {
            return AnyView(EmptyView())
        }
    }
    
    private func metaDevice() -> AnyView {
        if let proofData = proofData
        {
            let hw = proofData["Hardware"] ?? ""
            let man = proofData["Manufacturer"] ?? ""
            let lang = proofData["Language"] ?? ""
            let locale = proofData["Locale"] ?? ""
            return metadataView("ic_device", title: "\(man) \(hw)", text: "\(lang) \((!lang.isEmpty && !locale.isEmpty) ? ", " : "") \(locale)")
        } else {
            return AnyView(EmptyView())
        }
    }
    
    private func metaConnection() -> AnyView {
        if let proofData = proofData
        {
            let network = proofData["Network"] ?? ""
            let networkType = proofData["NetworkType"] ?? ""
            let cellInfo = proofData["CellInfo"] ?? ""
            return metadataView("ic_connection", title: "\(network) \(networkType)", text: "\(cellInfo)")
        } else {
            return AnyView(EmptyView())
        }
    }
    
    private func metadataView(_ icon: String, title: String, text: String) -> AnyView {
        return AnyView(HStack {
            Image(icon)
                .frame(width: metadataIconSize, height: metadataIconSize).padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 8))
            VStack(alignment: .leading) {
                if !title.isEmpty {
                    Text(title).fontWeight(.bold)
                }
                if !text.isEmpty {
                    Text(text)
                }
            }.padding(EdgeInsets(top: 8, leading: 0, bottom: 0, trailing: 0))
        })
    }
    
    
    private func update() {
        if let asset = asset {
            asset.getProofData { proofData in
                self.proofData = proofData
            }
        } else {
            self.proofData = nil
        }
        
        locationDisplayName = ""
        //        if let location = self.asset?.location {
        //            CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
        //                if error == nil, let placemark = placemarks?.first {
        //                    locationDisplayName = placemark.name ?? placemark.locality ?? placemark.subLocality ?? ""
        //                }
        //            }
        //        }
    }
}
