//
//  BundleItemsView.swift
//  Proofmode
//
//  Created by N-Pex on 2023-03-30.
//


import SwiftUI
import Photos
import LibProofMode
import AppTrackingTransparency
import ZIPFoundation

struct BundleItemsView: View {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @Environment(\.selectedAssets) var selectedAssets: [CameraItem]
    
    @State var operationText: String = ""
    @State var statusText: String = ""

    @State var sendMedia: Bool = true
    @State private var fileName = ""
    @State private var showOptionalBundleNameDialog: Bool = false

    var onBundlingDone: ((Any, String) -> Void)
    var onBundlingFailed: (() -> Void)

    var body: some View {
        VStack(alignment: .center, spacing: 15) {
            Text(operationText)
                .font(.custom(Font.titleFont, size: 21))
                .fontWeight(.bold)
                .foregroundColor(Color("colorDarkGray"))
                .multilineTextAlignment(.center)
                .padding()
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle())
            Text(statusText)
                .font(.custom(Font.bodyFont, size: 15))
                .fontWeight(.bold)
                .foregroundColor(Color("colorDarkGray"))
                .multilineTextAlignment(.center)
                .padding()
        }.padding(EdgeInsets(top: 10, leading: 0, bottom: 60, trailing: 0))
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(trailing:
                                    HStack {
                Button(action: {
                    withAnimation {
                        self.mode.wrappedValue.dismiss()
                    }
                }) {
                    Image(systemName: "xmark")
                        .imageScale(.large)
                }
                
            }
            )
            .alert(
                "Optional Filename",
                isPresented: self.$showOptionalBundleNameDialog
            ) {
                TextField("", text: $fileName)
                Button("Cancel") {
                    createZip(name: nil)
                }
                Button("OK") {
                    createZip(name: fileName)
                }.disabled($fileName.wrappedValue.isEmpty)
            } message: {
                Text("Set a custom name for the proof zip")
            }
            .onAppear {
                processItems()
            }
    }
    
    func processItems() {
        operationText = "Generating Proof"
        statusText = ""
        let unprocessedItems = self.selectedAssets.filter({ !$0.hasProof || $0.data == nil })
        if unprocessedItems.count > 0 {
            // If deviceID setting is "on" but we have no permission, ask!
            // TODO -  Move this!
//            if Settings.shared.optionPhone, ATTrackingManager.trackingAuthorizationStatus == .notDetermined {
//                ATTrackingManager.requestTrackingAuthorization { status in
//                    DispatchQueue.main.async {
//                        if status != .authorized {
//                            Settings.shared.optionPhone = false
//                        }
//                        processItems() // Iterate back, without option phone
//                    }
//                }
//                return
//            }
            var countDone = 0
            for item in unprocessedItems {
                if !item.isGeneratingProof {
                    // If not already generating proof
                    item.forceGenerateProof = false
                    Proof.shared.process(mediaItem: item, options: Settings.shared.toProofGenerationOptions()) { _ in
                        countDone += 1
                        statusText = "\(countDone) of \(self.selectedAssets.count)"
                    }
                }
            }
            Proof.shared.whenDone {
                // When done means the proof generation quque is empty = all newly processed and those that
                // were already in "isGeneratingProof" are done now.
                proofBuilt()
            }
        } else {
            DispatchQueue.main.async {
                proofBuilt()
            }
        }
    }
    
    func proofBuilt() {
        operationText = ""
        statusText = ""
        showOptionalBundleNameDialog = true
    }
    
    func createZip(name: String?) {
        operationText = "Bundling files"
        statusText = ""
        DispatchQueue.global(qos: .userInitiated).async {
            if let item = ProofZipHelper.createProofZip(fileName: fileName, assets: self.selectedAssets, addMedia: self.sendMedia) {
                DispatchQueue.main.async {
                    self.onBundlingDone(item.0, item.0.lastPathComponent)
                }
            } else {
                DispatchQueue.main.async {
                    self.onBundlingFailed()
                }
            }
        }
    }
}

struct BundleItemsView_Previews: PreviewProvider {
    static var previews: some View {
        BundleItemsView(onBundlingDone: {_,_ in}, onBundlingFailed: {})
    }
}
