//
//  BackButton.swift
//  Proofmode
//
//  Created by N-Pex on 2023-04-14.
//

import SwiftUI

struct BackButtonViewModifier: ViewModifier {
    @Environment(\.presentationMode) var presentationMode
    @State var title: String = ""
    
    func body(content: Content) -> some View {
        if title == "" {
            content
        } else {
            content
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading:
                                        Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    HStack {
                        Image(systemName: "chevron.backward")
                        Text(self.title)
                    }
                })
        }
    }
}

extension View {
    func customBackButton(title: String) -> some View {
        modifier(BackButtonViewModifier(title: title))
    }
}
