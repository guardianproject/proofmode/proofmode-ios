//
//  AppDelegate.swift
//  Proofmode
//
//  Created by N-Pex on 2020-07-15.
//

import UIKit
import SwiftUI
import LibProofMode

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        #if DEBUG
        let args = ProcessInfo.processInfo.arguments
        if args.contains("UI_TEST_MODE_RESET_SETTINGS") {
            // For testing mode, reset all settings!
            let defaults = UserDefaults(suiteName: "group.org.witness.proofmode.ios") ?? UserDefaults.standard
            defaults.reset()
        }
        #endif
        
        // Override point for customization after application launch.
        Proof.shared.initializeWithDefaultKeys()
        Proof.shared.synchronousNotarization = false
        
        // this is not the same as manipulating the proxy directly
        let appearance = UINavigationBarAppearance()
        
        // this overrides everything you have set up earlier.
        appearance.configureWithTransparentBackground()
                
        //In the following two lines you make sure that you apply the style for good
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        UINavigationBar.appearance().standardAppearance = appearance
        
        // This property is not present on the UINavigationBarAppearance
        // object for some reason and you have to leave it til the end
        UINavigationBar.appearance().tintColor = .gray
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

extension Settings {
    static var openTimestampsNotarizationProvider: OpenTimestampsNotarizationProvider?
    func toProofGenerationOptions() -> ProofGenerationOptions {
        if Settings.openTimestampsNotarizationProvider == nil {
            Settings.openTimestampsNotarizationProvider = OpenTimestampsNotarizationProvider()
        }
        let options = ProofGenerationOptions(showDeviceIds: self.optionPhone, showLocation: self.optionLocation, showMobileNetwork: self.optionNetwork, notarizationProviders: [
            Settings.openTimestampsNotarizationProvider ?? OpenTimestampsNotarizationProvider(),
            DeviceCheckNotarizationProvider(appIntegrityKey: self.appIntegrityKey)
        ])
        return options
    }
}
