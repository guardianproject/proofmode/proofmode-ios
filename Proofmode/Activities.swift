//
//  Activities.swift
//  Proofmode
//
//  Created by N-Pex on 2023-02-27.
//

import SwiftUI
import Foundation
import LibProofMode
import UniformTypeIdentifiers
import CoreData

struct ActivityLogHeader: Codable {
    var version: Int
    
    enum CodingKeys: String, CodingKey {
        case version
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(version, forKey: CodingKeys.version)
    }
    
    init(version: Int) {
        self.version = version
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.version = try container.decode(Int.self, forKey: CodingKeys.version)
    }
}

@objc
final class DbItem: NSManagedObject {
    lazy var cameraItem: CameraItem? = {
        var ci: CameraItem? = nil
        if let assetIdentifier = assetIdentifier {
            ci = CameraItem(mediaItemHash: mediaItemHash, assetIdentifier: assetIdentifier)
        } else if let url = url {
            ci = CameraItem(mediaItemHash, mediaUrl: url)
        }
        if let ci = ci {
            ci.id = self.objectID.uriRepresentation().absoluteString
            ci.modifiedDate = date
            if let mediaType = mediaType {
                ci.mediaType = UTType(mediaType)
            }
        }
        return ci
    }()
}

enum ActivityTypeEnum: Int16 {
    case captured = 0
    case imported = 1
    case shared = 2
    case sharePublicKey = 3
}

@objc
class Activity: NSManagedObject, Codable {
    
    var type: ActivityType = ActivityType()
    
    enum CodingKeys: String, CodingKey {
        case id, type, startTime
    }

    public static func == (lhs: Activity, rhs: Activity) -> Bool {
        return lhs.id == rhs.id
    }

    private func initializeType() {
        if self.activityType == ActivityTypeEnum.captured.rawValue {
            let items = (self.items?.sortedArray(using: [NSSortDescriptor(key: "date", ascending: true)]) as? [DbItem]) ?? []
            self.type = ActivityTypeCaptured(items: items.compactMap({ $0.cameraItem }))
        } else if self.activityType == ActivityTypeEnum.imported.rawValue {
            let items = (self.items?.sortedArray(using: [NSSortDescriptor(key: "date", ascending: true)]) as? [DbItem]) ?? []
            self.type = ActivityTypeImported(items: items.compactMap({ $0.cameraItem }))
        } else if self.activityType == ActivityTypeEnum.shared.rawValue, let fileName = self.fileName {
            let items = (self.items?.sortedArray(using: [NSSortDescriptor(key: "date", ascending: true)]) as? [DbItem]) ?? []
            self.type = ActivityTypeShared(items: items.compactMap({ $0.cameraItem }), fileName: fileName)
        } else if self.activityType == ActivityTypeEnum.sharePublicKey.rawValue, let key = self.data {
            self.type = ActivityTypePublicKeyShared(key: key)
        }
    }
    
    private func toDb() -> Void {
        if let captured = type as? ActivityTypeCaptured {
            let items = captured.items.map { ci in
                let item = DbItem(context: self.managedObjectContext ?? Activities.shared.container.viewContext)
                item.date = ci.creationDate ?? self.startTime
                item.assetIdentifier = ci.localAssetIdentifier
                item.mediaItemHash = ci.mediaItemHash
                item.mediaType = ci.mediaType?.identifier ?? UTType.image.identifier
                item.url = ci.mediaUrl
                return item
            }
            self.activityType = ActivityTypeEnum.captured.rawValue
            self.items = NSSet(array: items)
        } else if let imported = type as? ActivityTypeImported {
            let items = imported.items.map { ci in
                let item = DbItem(context: self.managedObjectContext ?? Activities.shared.container.viewContext)
                item.date = ci.creationDate ?? self.startTime
                item.assetIdentifier = ci.localAssetIdentifier
                item.mediaItemHash = ci.mediaItemHash
                item.mediaType = ci.mediaType?.identifier ?? UTType.image.identifier
                item.url = ci.mediaUrl
                return item
            }
            self.activityType = ActivityTypeEnum.imported.rawValue
            self.items = NSSet(array: items)
        } else if let shared = type as? ActivityTypeShared {
            let items = shared.items.map { ci in
                let item = DbItem(context: self.managedObjectContext ?? Activities.shared.container.viewContext)
                item.date = ci.creationDate ?? self.startTime
                item.assetIdentifier = ci.localAssetIdentifier
                item.mediaItemHash = ci.mediaItemHash
                item.mediaType = ci.mediaType?.identifier ?? UTType.image.identifier
                item.url = ci.mediaUrl
                return item
            }
            self.activityType = ActivityTypeEnum.shared.rawValue
            self.items = NSSet(array: items)
            self.fileName = shared.fileName
        } else if let publicKeyShared = type as? ActivityTypePublicKeyShared {
            self.activityType = ActivityTypeEnum.sharePublicKey.rawValue
            self.data = publicKeyShared.key
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: CodingKeys.id)
        try container.encode(type, forKey: CodingKeys.type)
        try container.encode(startTime, forKey: CodingKeys.startTime)
    }
    
    override private init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
      super.init(entity: entity, insertInto: context)
        self.initializeType()
    }
    
    init(context managedObjectContext: NSManagedObjectContext, type: ActivityType, startTime: Date, id: String? = UUID().uuidString) {
        self.init(context: managedObjectContext)
        self.id = id
        self.startTime = startTime
        self.type = type
        self.toDb()
    }
    
    required init(from decoder: Decoder) throws {
        let managedObjectContext = Activities.shared.container.viewContext
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try container.decode(String.self, forKey: CodingKeys.id)
        
        // Check if this object is already migrated to the DB
        let fetchRequest = Activity.fetchRequest()
        fetchRequest.predicate = NSPredicate(
            format: "id = %@", id
        )
        if let res = try? managedObjectContext.fetch(fetchRequest), !res.isEmpty {
            // Already in the DB, ignore!
            print("Activity \(id) is already migrated!")
            throw ActivityError.alreadyMigrated
        }
        
        self.init(context: managedObjectContext)
        self.id = id
        self.startTime = try container.decode(Date.self, forKey: CodingKeys.startTime)
        if let captured = try? container.decode(ActivityTypeCaptured.self, forKey: CodingKeys.type) {
            self.type = captured
        } else if let imported = try? container.decode(ActivityTypeImported.self, forKey: CodingKeys.type) {
            self.type = imported
        } else if let shared = try? container.decode(ActivityTypeShared.self, forKey: CodingKeys.type) {
            self.type = shared
        } else if let shared = try? container.decode(ActivityTypePublicKeyShared.self, forKey: CodingKeys.type) {
            self.type = shared
        }
        self.toDb()
    }
}

@objc
class ActivityType: NSObject, Codable {
    enum CodingKeys: String, CodingKey {
        case capture, mediaImport, mediaShare, publicKeyShare
    }

    enum MediaShareCodingKeys: String, CodingKey {
        case items, filename, importedItems
    }

    enum KeyShareCodingKeys: String, CodingKey {
        case publicKey
    }
    
    override init() {
        super.init()
    }
    
    required public init(from decoder: Decoder) throws {
        throw ActivityError.failedToDecodeActivity
    }
    
    public func encode(to encoder: Encoder) throws {
        throw ActivityError.failedToDecodeActivity
    }
}

class ActivityTypeCaptured: ActivityType {
    var items: [CameraItem]
    
    init(items: [CameraItem]) {
        self.items = items
        super.init()
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let capturedItems = try? container.decode([CameraItem].self, forKey: CodingKeys.capture) {
            self.items = capturedItems
            super.init()
            return
        }
        throw ActivityError.failedToDecodeActivity
    }
    
    override public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(items, forKey: CodingKeys.capture)
    }
}

class ActivityTypeImported: ActivityType {
    var items: [CameraItem]

    init(items: [CameraItem]) {
        self.items = items
        super.init()
    }

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let importedItems = try? container.decode([CameraItem].self, forKey: CodingKeys.mediaImport) {
            self.items = importedItems
            super.init()
            return
        }
        throw ActivityError.failedToDecodeActivity
    }
    
    override public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(items, forKey: CodingKeys.mediaImport)
    }
}

class ActivityTypeShared: ActivityType {
    var items: [CameraItem]
    var fileName: String
    
    init(items: [CameraItem], fileName: String) {
        self.items = items
        self.fileName = fileName
        super.init()
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let shareContainer = try? container.nestedContainer(keyedBy: MediaShareCodingKeys.self, forKey: CodingKeys.mediaShare) {
            self.items = try shareContainer.decode([CameraItem].self, forKey: MediaShareCodingKeys.items)
            self.fileName = try shareContainer.decode(String.self, forKey: MediaShareCodingKeys.filename)
            super.init()
            return
        }
        throw ActivityError.failedToDecodeActivity
    }
    
    override public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var shareContainer = container.nestedContainer(keyedBy: MediaShareCodingKeys.self, forKey: CodingKeys.mediaShare)
        try shareContainer.encode(items, forKey: MediaShareCodingKeys.items)
        try shareContainer.encode(fileName, forKey: MediaShareCodingKeys.filename)
    }
}

class ActivityTypePublicKeyShared: ActivityType {
    var key: String
    
    init(key: String) {
        self.key = key
        super.init()
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let shareContainer = try? container.nestedContainer(keyedBy: KeyShareCodingKeys.self, forKey: CodingKeys.publicKeyShare) {
            self.key = try shareContainer.decode(String.self, forKey: KeyShareCodingKeys.publicKey)
            super.init()
            return
        }
        throw ActivityError.failedToDecodeActivity
    }
    
    override public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var shareContainer = container.nestedContainer(keyedBy: KeyShareCodingKeys.self, forKey: CodingKeys.publicKeyShare)
        try shareContainer.encode(key, forKey: KeyShareCodingKeys.publicKey)
    }
}

public enum ActivityError: Error {
    case failedToDecodeActivity
    case failedToWriteActivityLog(error: String)
    case alreadyMigrated
}

extension CodingUserInfoKey {
    static var fileVersion = CodingUserInfoKey(rawValue: "fileVersion")
}

class ActivitiesJSONDecoder: JSONDecoder {
    override init() {
        super.init()
    }
    
    var fileVersion: Int {
        get {
            if let key = CodingUserInfoKey.fileVersion {
                return self.userInfo[key] as? Int ?? 0
            }
            return 0
        }
        set(value) {
            if let key = CodingUserInfoKey.fileVersion {
                self.userInfo[key] = value
            }
        }
    }
}

class Activities: ObservableObject {
    static let shared = Activities()
    static let currentFileVersion: Int = 1

    let container = NSPersistentContainer(name: "ActivityDataModel")
        
    var groupedForDisplayMoc: NSManagedObjectContext
    var tempMoc: NSManagedObjectContext
    
    @Published var loaded = false
    
    /**
        A date value, that acts as an event for us to "scrollToTop" in the activities view. We update this whenever a scroll needs to happen, like when we add temp activity or
     when scene codes to the foreground.
     */
    @Published var lastUpdated: Date = Date.now
        
    init() {
        groupedForDisplayMoc = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        groupedForDisplayMoc.parent = container.viewContext
        groupedForDisplayMoc.automaticallyMergesChangesFromParent = false

        tempMoc = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        tempMoc.parent = container.viewContext
        tempMoc.automaticallyMergesChangesFromParent = true

        let storeURL = getDocumentsDirectory().appendingPathComponent("proofmodedb.sqlite")
        let storeDescription = NSPersistentStoreDescription(url: storeURL)
        

        
        container.persistentStoreDescriptions = [storeDescription]
        container.loadPersistentStores { description, error in
            if let error = error {
                print("Core Data failed to load: \(error.localizedDescription)")
            }
            self.updateActivitiesGroupedForDisplay()
            self.updateAllCapturedAndImportedItems()

            DispatchQueue.main.async {
                self.loaded = true
                self.migrateToDBObjects()
                NotificationCenter.default.addObserver(self, selector: #selector(self.managedObjectContextUpdated(_:)), name: Notification.Name.NSManagedObjectContextDidSave, object: nil)
            }
        }
        
        if EnvironmentValues.gIsPreview {
            var items: [CameraItem] = []
            if let data = UIImage(named: "img_home-off")?.pngData() {
                items.append(CameraItem(nil, mediaData: data, mediaType: UTType.image))
            }
            if let data = UIImage(named: "img_home-on")?.pngData() {
                items.append(CameraItem(nil, mediaData: data, mediaType: UTType.image))
            }
            Activities.createActivity(type: ActivityTypeCaptured(items: items), startTime: Date.now)
        }
    }
    
    @objc
    func managedObjectContextUpdated(_ notification: Notification) {
        self.updateActivitiesGroupedForDisplay()
        self.updateAllCapturedAndImportedItems()
    }
    
    func getDocumentsDirectory() -> URL {
        return FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.org.witness.proofmode.ios")
         ?? FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    
    func migrateToDBObjects() {
        DispatchQueue.global(qos: .userInitiated).async {
            do {
                let url = self.getDocumentsDirectory().appendingPathComponent("activities.log")
                guard FileManager.default.fileExists(atPath: url.path) else { return }
                
                let data = try String(contentsOf: url, encoding: .utf8)
                var jsonObjects = data.components(separatedBy: .newlines)
                let decoder = ActivitiesJSONDecoder()
                
                // Try to read header
                var fileVersion = 0
                if let header = jsonObjects.first, let data = header.data(using: .utf8), let activityLogHeader = try? decoder.decode(ActivityLogHeader.self, from: data) {
                    fileVersion = activityLogHeader.version
                    jsonObjects = Array(jsonObjects.dropFirst(1))
                }
                
                if fileVersion != Activities.currentFileVersion {
                    print("Migration needed")
                }
                decoder.fileVersion = fileVersion
                
                jsonObjects.forEach { s in
                    if let data = s.data(using: .utf8) {
                        DispatchQueue.main.async {
                            if let _ = try? decoder.decode(Activity.self, from: data) {
                                print("Created migrated activity")
                                //self.activities.append(activity)
                            }
                        }
                    }
                }
                
                try FileManager.default.removeItem(atPath: url.path)
            } catch {}
            DispatchQueue.main.async {
                do {
                    if Activities.shared.container.viewContext.hasChanges {
                        try Activities.shared.container.viewContext.save()
                    }
                } catch {
                    print(error)
                }
                self.objectWillChange.send()
            }
        }
    }
        
    static func createActivity(type: ActivityType, startTime: Date) -> Void {
        DispatchQueue.main.async {
            do {
                let _ = Activity(context: Activities.shared.container.viewContext, type: type, startTime: startTime)
                if Activities.shared.container.viewContext.hasChanges {
                    try Activities.shared.container.viewContext.save()
                }
                //Activities.shared.objectWillChange.send()
            } catch {
                print(error)
            }
        }
    }

    static func createTempActivity(type: ActivityType, startTime: Date, context: NSManagedObjectContext) -> Activity {
        let act = Activity(context: context, type: type, startTime: startTime)
        Self.shared.updateActivitiesGroupedForDisplay()
        Self.shared.updateAllCapturedAndImportedItems()
        Self.shared.lastUpdated = Date.now
        return act
    }

    @Published public var allCapturedAndImportedItems: [CameraItem] = []
    
    /**
     Consolidate activities by grouping together consecutive "capture" activities that are taken on the same day.
     */
    @Published public var activitiesGroupedForDisplay: [Activity] = []
    
    func updateActivitiesGroupedForDisplay() {
        // Reset the display context
        groupedForDisplayMoc = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        groupedForDisplayMoc.parent = container.viewContext
        groupedForDisplayMoc.automaticallyMergesChangesFromParent = false
        
        let request: NSFetchRequest<Activity> = NSFetchRequest(entityName: "Activity")
        request.sortDescriptors = [NSSortDescriptor(key: "startTime", ascending: true)]
        let activities = (try? tempMoc.fetch<Activity>(request)) ?? []
        
        var updatedActivities: [Activity] = []
        
        func isSameDay(date1: Date, date2: Date) -> Bool {
            let diff = Calendar.current.dateComponents([.day], from: date1, to: date2)
            if diff.day == 0 {
                return true
            } else {
                return false
            }
        }
        
        activities.forEach { activity in
            var updatedActivity = activity
            if let mediaCaptured = activity.type as? ActivityTypeCaptured {
                if let previous = updatedActivities.last {
                    if let previousCaptured = previous.type as? ActivityTypeCaptured {
                        if let d1 = activity.startTime, let d2 = previous.startTime, isSameDay(date1: d1, date2: d2) {
                            // Join them, but only if not from today (for today we keep items separated so you can easily find "photos you just captured".
                            if !d1.isFromLatestDay() {
                                updatedActivity = Activity(context: groupedForDisplayMoc, type: ActivityTypeCaptured(items: previousCaptured.items + mediaCaptured.items), startTime: d2, id: previous.id)
                                let _ = updatedActivities.popLast()
                            }
                        }
                    }
                }
            }
            updatedActivities.append(updatedActivity)
        }
        self.activitiesGroupedForDisplay = updatedActivities.reversed()
    }
    
    func updateAllCapturedAndImportedItems() {
        var seen: Set<String> = []

        // TODO - cache
        let request: NSFetchRequest<Activity> = NSFetchRequest(entityName: "Activity")
        request.sortDescriptors = [NSSortDescriptor(key: "startTime", ascending: true)]
        
        let activities = (try? tempMoc.fetch<Activity>(request)) ?? []
        let items:[CameraItem]? = try? activities.reduce<[CameraItem]>([], { partialResult, item in
            if let captured = item.type as? ActivityTypeCaptured {
                return partialResult + captured.items
            } else if let imported = item.type as? ActivityTypeImported {
                return partialResult + imported.items
            }
            return partialResult
        })
        // Return unique and remove deleted
        self.allCapturedAndImportedItems = items?.filter({ item in
            if case .deleted(_) = item.state { return false }
            else if let identifier = item.localAssetIdentifier { return seen.insert(identifier).inserted }
            else { return true }
        }) ?? []
    }
}
