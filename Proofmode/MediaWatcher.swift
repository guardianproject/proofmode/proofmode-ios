//
//  MediaWatcher.swift
//  Proofmode
//
//  Created by N-Pex on 2020-07-22.
//

import Foundation
import Photos
import LibProofMode

class MediaWatcher: NSObject, PHPhotoLibraryChangeObserver {
    static let shared = MediaWatcher()
    
    var lastPhotoLibraryFetchResult:PHFetchResult<PHAsset>? = nil
    
    fileprivate override init() {
        super.init()
        PHPhotoLibrary.requestAuthorization { (status) in
            if status == .authorized {
                let fetchOptions = PHFetchOptions()
                fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                self.lastPhotoLibraryFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                PHPhotoLibrary.shared().register(self)
            }
        }
    }
    
    func update() {
        //TODO - When starting, figure out what images we haven't processed yet and add those...
    }
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        guard
            let last = lastPhotoLibraryFetchResult,
            let fetchResultChangeDetails = changeInstance.changeDetails(for: last) else {
                return;
        }
        lastPhotoLibraryFetchResult = fetchResultChangeDetails.fetchResultAfterChanges
        guard fetchResultChangeDetails.insertedObjects.count > 0 else {return} // No new ones

        print("Processing new media items(s)")

        // Is automatic proof generation on?
        if Settings.shared.isOn {
            for asset in fetchResultChangeDetails.insertedObjects {
                print("Processing media item \(asset.localIdentifier)")
                //Proof.shared.process(mediaItem: MediaItem(asset: asset), options: Settings.shared.toProofGenerationOptions())
            }
            Proof.shared.whenDone {
                print("Processing done")
            }
        }
    }
}
