//
//  OpenTimestampsNotarizationProvider.swift
//  Proofmode
//
//  Created by N-Pex on 2022-07-21.
//

import Foundation
import WebKit
import LibProofMode

class OpenTimestampsNotarizationProvider : NSObject, NotarizationProvider, WKScriptMessageHandler, WKNavigationDelegate {
    
    private var webView: WKWebView?
    private var onSuccess: ((String, String) -> Void)?
    private var onFailure: ((Int, String) -> Void)?
    
    var fileExtension: String = ".ots"
    private var mainNavigation: WKNavigation?
    private var mediaHash: String = ""
    
    func notarize(hash: String, media: Data, success: @escaping (String, String) -> Void, failure: @escaping (Int, String) -> Void) {
        self.onSuccess = success
        self.onFailure = failure
        self.mediaHash = hash
        
        guard let jsFilePath = Bundle.main.path(forResource: "opentimestamps.min", ofType: "js"), let js = try? String(contentsOfFile: jsFilePath) else {
            failure(0, "No JS file found!")
            return
        }
        
        DispatchQueue.main.async {
            if self.webView != nil, self.mainNavigation != nil {
                // Just call the script
                let s = "window.stamp('\(self.mediaHash)');"
                self.webView?.evaluateJavaScript(s)
                return
            }
            self.webView = WKWebView()
            self.webView?.configuration.userContentController.add(self, name: "callbackSuccess")
            self.webView?.configuration.userContentController.add(self, name: "callbackFailure")
            self.webView?.navigationDelegate = self
            let content = """
                    <html>
                        <head>
                            <script>\(js)</script>
                            <script>
                                const fromHexString = function (hexString) {
                                   var result = [];
                                   while (hexString.length >= 2) {
                                       result.push(parseInt(hexString.substring(0, 2), 16));
                                       hexString = hexString.substring(2, hexString.length);
                                   }
                                   return result;
                                };
                                window.stamp = function(inhash) {
                                    try {
                                        const hash = fromHexString(inhash);
                                        var detached = OpenTimestamps.DetachedTimestampFile.fromHash(new OpenTimestamps.Ops.OpSHA256(), hash);
                                        OpenTimestamps.stamp([detached], { calendars: [], m: 1 })
                                            .then(() => {
                                                const json = { bytes: OpenTimestamps.Utils.arrayToBytes(detached.serializeToBytes()) };
                                                const result = JSON.stringify(json);
                                                window.webkit.messageHandlers.callbackSuccess.postMessage({'result': result, 'hash': inhash});
                                            })
                                            .catch(err => {
                                                window.webkit.messageHandlers.callbackFailure.postMessage({'errorMessage': 'Failed to stamp!', 'error': err});
                                            });
                                    } catch (err) {
                                        window.webkit.messageHandlers.callbackFailure.postMessage({'error': err});
                                    }
                                };
                            </script>
                        </head>
                        <body>
                        </body>
                    </html>
"""
            self.mainNavigation = self.webView?.loadHTMLString(content, baseURL: nil)
        }
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "callbackSuccess" {
            if let dict = message.body as? [String:String],
               let hash = dict["hash"],
               let resultJson =  dict["result"],
               let d = resultJson.data(using: .utf8),
               let o = try? JSONSerialization.jsonObject(with: d) as? [String:[Int]],
               let b = o["bytes"] {
                let data = Data(b.map { inputInteger in
                    UInt8(inputInteger)
                })
                self.onSuccess?(hash,data.base64EncodedString())
            }
            else {
                self.onFailure?(0, "OpenTimestamps notarization failed!")
            }
        } else if message.name == "callbackFailure" {
            self.onFailure?(0, "OpenTimestamps notarization failed!")
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if navigation === mainNavigation {
            let s = "window.stamp('\(self.mediaHash)');"
            self.webView?.evaluateJavaScript(s)
        }
    }
}
