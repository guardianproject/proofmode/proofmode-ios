//
//  DeviceCheckNotarizationProvider.swift
//  Proofmode
//
//  Created by N-Pex on 2023-01-12.
//

import Foundation
import WebKit
import DeviceCheck
import CryptoKit
import LibProofMode

class DeviceCheckNotarizationProvider : NSObject, NotarizationProvider {
    
    private var appIntegrityKey: String?
    private var onSuccess: ((String, String) -> Void)?
    private var onFailure: ((Int, String) -> Void)?

    init(appIntegrityKey: String?) {
        super.init()
        self.appIntegrityKey = appIntegrityKey
    }
    
    var fileExtension: String = ".devicecheck"
    
    func notarize(hash: String, media: Data, success: @escaping (String, String) -> Void, failure: @escaping (Int, String) -> Void) {
        self.onSuccess = success
        self.onFailure = failure
            
        if DCAppAttestService.shared.isSupported {
            if let data = hash.data(using: .utf8), let appIntegrityKey = self.appIntegrityKey {
                DCAppAttestService.shared.attestKey(appIntegrityKey, clientDataHash: Data(SHA256.hash(data: data))) { attestation, error in
                    guard error == nil, let attestation = attestation else {
                        DispatchQueue.main.async {
                            failure(0, "Error")
                        }
                        return
                    }
                    DispatchQueue.main.async {
                        success(hash, attestation.base64EncodedString())
                    }
                }
            } else {
                DispatchQueue.main.async {
                    failure(0, "Failed to create data")
                }
            }
        } else {
            DispatchQueue.main.async {
                failure(0, "DCAppAttestService not supported")
            }
        }
    }
}
