//
//  VideoPlayerViewModel.swift
//  Proofmode
//
//  Created by N-Pex on 2023-09-22.
//

import SwiftUI
import AVKit

class VideoPlayerViewModel: NSObject, ObservableObject {
    @Published var avPlayer: AVPlayer?
    @Published var currentAsset: CameraItem?
    @Published var showPlayer: Bool = false
    
    func isPlaying() -> Bool {
        return showPlayer && avPlayer != nil
    }
    
    func loadAsset(_ asset: CameraItem?) {
        if let asset = asset, let currentAsset = currentAsset, asset.mediaItemHash == currentAsset.mediaItemHash {
            return
        }

        // Cleanup old files and observers
        deleteTempVideoFiles()
        NotificationCenter.default.removeObserver(self)
        
        self.currentAsset = asset
        self.showPlayer = false
        if let asset = asset, asset.asset?.mediaType == .video {
            if let url = asset.mediaUrl {
                self.loadFromUrl(url: url)
            } else {
                asset.withData { data in
                    if let data = data, let url = self.saveTempVideoFile(data, filenameExtension: asset.mediaType?.preferredFilenameExtension ?? "mp4") {
                        self.loadFromUrl(url: url)
                    }
                }
            }
        } else {
            self.avPlayer = nil
        }
    }
    
    func play() {
        avPlayer?.seek(to: .zero)
        showPlayer = true
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        showPlayer = false
    }

    @objc func playerFailedFinishPlaying(note: NSNotification) {
        showPlayer = false
    }

    private func loadFromUrl(url: URL) {
        DispatchQueue.main.async {
            self.avPlayer = AVPlayer(url: url)
            if let player = self.avPlayer {
                player.actionAtItemEnd = .pause
                NotificationCenter.default
                    .addObserver(self,
                                 selector: #selector(self.playerDidFinishPlaying),
                                 name: .AVPlayerItemDidPlayToEndTime,
                                 object: player.currentItem
                    )
                NotificationCenter.default
                    .addObserver(self,
                                 selector: #selector(self.playerFailedFinishPlaying),
                                 name: .AVPlayerItemFailedToPlayToEndTime,
                                 object: player.currentItem
                    )
            }
        }
    }
        
    func deleteTempVideoFiles() {
        if let directoryContents = try? FileManager.default.contentsOfDirectory(
            at: FileManager.default.temporaryDirectory,
                includingPropertiesForKeys: nil
        ) {
            for url in directoryContents {
                if url.lastPathComponent.hasPrefix("tempvideo") {
                    do {
                        try FileManager.default.removeItem(at: url)
                    } catch {
                    }
                }
            }
        }
    }
    
    func saveTempVideoFile(_ data: Data, filenameExtension: String) -> URL?
    {
        let tempURL = FileManager.default.temporaryDirectory.appendingPathComponent("tempvideo").appendingPathExtension(filenameExtension)
        do {
            try data.write(to: tempURL, options: [.atomic])
        } catch {
            print()
        }
        return tempURL
    }
}
