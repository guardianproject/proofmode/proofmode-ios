//
//  ContentView.swift
//  Proofmode
//
//  Created by N-Pex on 2020-07-15.
//

import SwiftUI
import MobileCoreServices
import Photos
import PhotosUI
import UniformTypeIdentifiers
import LibProofMode
import AppTrackingTransparency
import CameraManager
import CoreData

extension Notification.Name {
    static let selectionClearedNotification = Notification.Name("SelectionClearedNotification")
}

public extension ShapeStyle where Self == Color {
    static var debug: Color {
    #if DEBUG
        return Color(
            red: .random(in: 0...1),
            green: .random(in: 0...1),
            blue: .random(in: 0...1)
        )
    #else
        return Color(.clear)
    #endif
    }
}

enum NavigationScreen {
    case home
    case media
    case more
    case settings
    case howItWorks
    case dataLegend
    case digitalSignatures
    
    static func getBinding(navigationPath:Binding<[NavigationScreen]>, screen:NavigationScreen) -> Binding<Bool> {
        return Binding(get: {
            return navigationPath.wrappedValue.last == screen
        }, set: { val in
            if (!val && navigationPath.wrappedValue.last == screen) {
                let _ = navigationPath.wrappedValue.popLast()
            }
            else if (val && !navigationPath.wrappedValue.contains(where: { s in
                s == screen
            })) {
                navigationPath.wrappedValue.append(screen)
            }
        })
    }
}

public class MediaItemCollection: ObservableObject {
    init(mediaItems:[MediaItem]) {
        self.mediaItems = mediaItems
    }
    
    // MARK: -
    @Published var mediaItems: [MediaItem]
}

enum ShareItemInfo {
    case shareNothing
    /**
     url is set for re-share of original .zip file, otherwise nil
     */
    case shareMedia(assets: [CameraItem], url: URL? = nil)
    case shareC2PASignedMedia(assets: [CameraItem])
    case sharePublicKey(item: String)
}

enum ImportInfo {
    case nothing
    case importedAll(items: [CameraItem])
    case importedNone(items: [CameraItem], authStatus: PHAuthorizationStatus)
    case importedSome(items: [CameraItem], authStatus: PHAuthorizationStatus)
}

struct NavigationContainer<Content: View>: View {
    var content: () -> Content

    init(@ViewBuilder content: @escaping () -> Content) {
        self.content = content
    }

    var body: some View {
        if #available(iOS 16.0, *) {
            NavigationStack(root: content)
        } else {
            // Fallback on earlier versions
            NavigationView(content: content).navigationViewStyle(.stack)
        }
    }
}

struct MainView: View {
    @Environment(\.onNavigateToScreen) private var onNavigateToScreen
    @Environment(\.managedObjectContext) private var moc
    @State var geo:GeometryProxy
    @State private var dragOffset: CGFloat = 0
    @Binding var showCamera:Bool
    @StateObject var cameraManager =  CameraManagerObserver(cameraManager: OrientationFixedCameraManager())
    
    let cameraAvailable = UIImagePickerController.isSourceTypeAvailable(.camera)
    
    var body: some View {
        HStack(alignment: .top, spacing: 0) {
            activitiesView
                .frame(width: geo.size.width, height: geo.size.height)

            if cameraAvailable {
                cameraView
                    .frame(width: geo.size.width, height: geo.size.height)
            }
        }
        .id("mainView")
        .frame(width: (cameraAvailable ? 2 : 1) * geo.size.width, height: geo.size.height)
        .offset(x: -geo.size.width * dragOffset)
        .if (cameraAvailable) { view in
            view.modifier(DragGestureViewModifier(
                onUpdate: { val in
                    let offset = val.translation.width / geo.size.width
                    dragOffset = max(0, min(1, (showCamera ? 1 : 0) - offset))
                },
                onEnd: { maybeVal in
                    if let val = maybeVal, val.translation.width < -50, !showCamera {
                        showCamera = true
                    } else if let val = maybeVal, val.translation.width > 50, showCamera {
                        showCamera = false
                    } else {
                        withAnimation(.spring()) {
                            dragOffset = showCamera ? 1 : 0
                        }
                    }
                }
            )
            )
        }
        .onChange(of: showCamera) { show in
            if !show {
                let session = cameraManager.currentSession
                var tempActivity: Activity? = nil
                if session.itemsBeingProcessed {
                    // Insert temporary placeholder in UI, while
                    // we sign and save and get the correct assets identifiers.
                    let items = session.cameraItemsOrdered
                    guard items.count > 0 else { return }
                    var startTime = Date.now
                    if let item = items.first, let startDate = item.modifiedDate {
                        startTime = startDate
                    }
                    DispatchQueue.main.async {
                        tempActivity = Activities.createTempActivity(type: ActivityTypeCaptured(items: items), startTime: startTime, context: moc)
                    }
                }
                session.whenSaved { items in
                    guard items.count > 0 else { return }
                    var startTime = Date.now                            
                    if let item = items.first, let startDate = item.modifiedDate {
                        startTime = startDate
                    }
                    DispatchQueue.main.async {
                        if let tempActivity = tempActivity {
                            moc.delete(tempActivity)
                        }
                        let _ = Activities.createActivity(type: ActivityTypeCaptured(items: items), startTime: startTime)
                        if tempActivity == nil {
                            // If no temp was created, scroll to top now (since we did not do it earlier)
                            Activities.shared.lastUpdated = Date.now
                        }
                        items.forEach { ci in
                            ci.generateProof()
                        }
                    }
                }
            } else if show {
                // Start new session
                cameraManager.startNewCaptureSession()
            }
            withAnimation {
                dragOffset = show ? 1 : 0
            }
        }
    }
    
    private var activitiesView: some View {
        VStack {
            HStack(alignment: .center) {
                Spacer()
                Button(action: {
                    onNavigateToScreen?(.more)
                }) {
                    Image(systemName: "ellipsis")
                        .frame(width: 32, height: 32)
                        .foregroundColor(Color.black)
                        .clipped()
                }.accessibilityIdentifier("menuButton")
            }
            .padding()
            .frame(height: 50)
            
            ActivitiesView(onCameraButton: {
                withAnimation {
                    self.showCamera = true
                }
            })
        }
        .id("activitiesView")
        .background(.white)
        .zIndex(0)
    }
    
    private var cameraView: some View {
        return ZStack {
            CameraView(cameraManager: self.cameraManager,
                       showCamera: $showCamera,
                       onDone: {
                withAnimation {
                    self.showCamera = false
                }})
            .id("cameraView")
            
            // This is just a small drag handle running along the left screen edge
            // so that screen swipes back to activity view will work smoothly.
            HStack {
                ZStack {
                }
                .frame(minWidth: 10, maxWidth: 10, minHeight: 0, maxHeight: .infinity)
                .background(.black.opacity(0.01))
                Spacer()
            }
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        }
        .zIndex(1)
    }
    
    private func saveCameraItems() {
//        if cameraManager.cameraItems.count > 0 {
//            // Save to activity log
//            DispatchQueue.main.async {
//                let _ = Activities.createActivity(type: ActivityTypeCaptured(items: cameraManager.cameraItems.sorted(by: { left, right in
//                    (left.creationDate?.timeIntervalSince1970 ?? 0) < (right.creationDate?.timeIntervalSince1970 ?? 0)
//                })), startTime: Date.now)
//                cameraManager.clear()
//            }
//        }
    }
}

struct ContentView: View {
    @EnvironmentObject<MediaItemCollection> var mediaItems: MediaItemCollection
    @EnvironmentObject var settings: Settings
    
    @Environment(\.isPreview) var isPreview
    
    @State private var navigationPath: [NavigationScreen] = [.home]
    
    @State var showCamera:Bool = false {
        didSet {
            if showCamera {
                // Reset when shown
                clearSelectedAssets()
            }
        }
    }
    @State var showPhotoPicker: Bool = false
    
    /**
     Whether or not to show share sheet. Do not set this directly, instead use "itemToShare" below. When set to something, the share sheet will be displayed. To remove, set itemToShare to .shareNothing.
     */
    @State var showShareSheet:Bool = false
    @State var itemToShare: ShareItemInfo = .shareNothing {
        didSet {
            DispatchQueue.main.async {
                if case .shareNothing = itemToShare {
                    showShareSheet = false
                    clearSelectedAssets()
                } else {
                    showShareSheet = true
                }
            }
        }
    }
    
//    @FetchRequest(sortDescriptors: [NSSortDescriptor(key: "startTime", ascending: false)])
//    var activities: FetchedResults<Activity>

    @State var importInfo: ImportInfo = .nothing
    @State var importInfoPending: ImportInfo = .nothing
    @State var selectedAsset: CameraItem? = nil
    @State var selectedAssets: [CameraItem] = []
    
    var body: some View {
        if !self.settings.onboarded {
            NavigationContainer {
                HowItWorksView(onboarding: true)
            }
        } else {
            NavigationContainer {
                if #available(iOS 16.0, *) {
                    GeometryReader { geo in
                        MainView(geo: geo, showCamera: $showCamera)
                            .preferredColorScheme(self.showCamera ? .dark : .light)
                            .environment(\.onAssetTapped, { asset in
                                if self.selectedAssets.count > 0 {
                                    self.toggleAssetSelected(asset)
                                } else {
                                    self.selectedAsset = asset
                                    self.navigationPath.append(.media)
                                }
                            })
                            .environment(\.onAssetLongTapped, { asset in
                                self.toggleAssetSelected(asset)
                            })
                            .environment(\.selectedAssets, self.selectedAssets)
                    }
                    .navigationBarHidden(true)
                    .navigationBarTitle("")
                    .navigationTitle("")
                    .navigationBarTitleDisplayMode(.inline)
                    .navigationDestination(isPresented: NavigationScreen.getBinding(navigationPath: $navigationPath, screen: .media)) {
                        self.singleAssetView
                    }
                    .navigationDestination(isPresented: NavigationScreen.getBinding(navigationPath: $navigationPath, screen: .more)) {
                        MoreView().customBackButton(title: "")
                    }
                    .navigationDestination(isPresented: NavigationScreen.getBinding(navigationPath: $navigationPath, screen: .settings)) {
                        SettingsView().customBackButton(title: "More")
                    }
                    .navigationDestination(isPresented: NavigationScreen.getBinding(navigationPath: $navigationPath, screen: .howItWorks)) {
                        HowItWorksView(onboarding: false).customBackButton(title: "More")
                    }
                    .navigationDestination(isPresented: NavigationScreen.getBinding(navigationPath: $navigationPath, screen: .dataLegend)) {
                        DataLegendView().customBackButton(title: "More")
                    }
                    .navigationDestination(isPresented: NavigationScreen.getBinding(navigationPath: $navigationPath, screen: .digitalSignatures)) {
                        DigitalSignaturesView().customBackButton(title: "More")
                    }
                } else {
                    // Fallback on earlier versions
                    GeometryReader { geo in
                        MainView(geo: geo, showCamera: $showCamera)
                        navigationScreen(screen: .media, destination: self.singleAssetView, backButtonTitle: "")
                        navigationScreen(screen: .more, destination: MoreView(), backButtonTitle: "")
                        navigationScreen(screen: .settings, destination: SettingsView(), backButtonTitle: "")
                        navigationScreen(screen: .howItWorks, destination: HowItWorksView(onboarding: false), backButtonTitle: "")
                        navigationScreen(screen: .dataLegend, destination: DataLegendView(), backButtonTitle: "")
                        navigationScreen(screen: .digitalSignatures, destination: DigitalSignaturesView(), backButtonTitle: "")
                            .preferredColorScheme(self.showCamera ? .dark : .light)
                            .environment(\.onAssetTapped, { asset in
                                if self.selectedAssets.count > 0 {
                                    self.toggleAssetSelected(asset)
                                } else {
                                    self.selectedAsset = asset
                                    self.navigationPath.append(.media)
                                }
                            })
                            .environment(\.onAssetLongTapped, { asset in
                                self.toggleAssetSelected(asset)
                            })
                            .environment(\.selectedAssets, self.selectedAssets)
                    }
                    .navigationBarHidden(true)
                    .navigationBarTitle("")
                    .navigationTitle("")
                    .navigationBarTitleDisplayMode(.inline)
                }
            }
            .id("mainNavigation")
            .environment(\.managedObjectContext, Activities.shared.tempMoc)
            .environment(\.onNavigateToScreen, self.onNavigateToScreen(screen:))
            .environment(\.onShareItem, onShareItem)
            .environment(\.onSharePublicKey, { publicKey in
                itemToShare = .sharePublicKey(item: publicKey)
            })
            .environment(\.onBundleItems, onBundleItems)
            .environment(\.onC2PAShareItems, onC2PAShareItems)
            .environment(\.onImportMedia, {
                clearSelectedAssets()
                self.showPhotoPicker = true
            })
            
            .sheet(isPresented: $showPhotoPicker, onDismiss: {
                importInfo = importInfoPending
                handleImportedItems()
            }) {
                PhotoPicker(parent: self, importInfo: self.$importInfoPending)
            }
            .sheet(isPresented: $showShareSheet, onDismiss: {
                itemToShare = .shareNothing
            }) {
                ShareSheetPopup(itemToShare: self.$itemToShare)
            }
            
            .modifier(ImportErrorModifier(importInfo: self.$importInfo))
        }
    }
    
    private func onNavigateToScreen(screen: NavigationScreen) {
        if #available(iOS 16.0, *) {
            if (!navigationPath.contains(where: { s in
                s == screen
            })) {
                navigationPath.append(screen)
            }
        } else {
            navigationPath = [screen]
        }
    }
    
    private func navigationScreen(screen: NavigationScreen, destination: some View, backButtonTitle: String) -> some View {
        NavigationLink(destination: destination.customBackButton(title: backButtonTitle), isActive: NavigationScreen.getBinding(navigationPath: $navigationPath, screen: screen)) {
            EmptyView()
        }
   }
    
    private func clearSelectedAssets() {
        self.selectedAssets.removeAll()
        NotificationCenter.default.post(name: Notification.Name.selectionClearedNotification, object: nil)
    }
    
    private func toggleAssetSelected(_ asset: CameraItem) {
        if self.selectedAssets.contains(where: { a in
            a.uniqueIdentifier == asset.uniqueIdentifier
        }) {
            self.selectedAssets.removeAll(where: { a in
                a.uniqueIdentifier == asset.uniqueIdentifier
            })
        } else {
            self.selectedAssets.append(asset)
        }
    }
    
    private func onShareItem(_ item: Any, forAssets assets: [CameraItem]) {
        self.itemToShare = .shareMedia(assets: assets, url: item as? URL)
    }
    
    private func onBundleItems(_ items: [CameraItem]) {
        self.itemToShare = .shareMedia(assets: items)
    }
    
    private func onC2PAShareItems(_ items: [CameraItem]) {
        self.itemToShare = .shareC2PASignedMedia(assets: items)
    }
    
    private var _singleAssetView: SingleAssetView = SingleAssetView(vm: SingleAssetViewViewModel())
    private var singleAssetView: some View {
        get {
            return _singleAssetView
                .onAppear {
                    let assets = Activities.shared.allCapturedAndImportedItems
                    var index = 0
                    if let asset = self.selectedAsset {
                        index = assets.firstIndex(of: asset) ?? assets.firstIndex(where: { e in
                            return (asset.localAssetIdentifier != nil && e.localAssetIdentifier == asset.localAssetIdentifier) || (asset.mediaUrl != nil && e.mediaUrl == asset.mediaUrl)
                        }) ?? 0
                    }
                    _singleAssetView.update(assets: assets, index: index)
                }
        }
    }
    
    private func sharePublicKey() {
        if let publicKeyString = Proof.shared.getPublicKeyString() {
            self.itemToShare = .sharePublicKey(item: publicKeyString)
        }
    }
    
    func handleImportedItems() {
        switch self.importInfo {
        case .importedAll(items: let items):
            // Generate proof for them, if needed!
            let options = Settings.shared.toProofGenerationOptions()
            items.forEach { item in
                item.forceGenerateProof = false
                Proof.shared.process(mediaItem: item, options: options)
            }
            
            // Save to activity log
            let _ = Activities.createActivity(type: ActivityTypeImported(items: items), startTime: Date.now)
            break
        default:
            break
        }
    }
}

struct ImportErrorModifier: ViewModifier {
    @Binding var importInfo: ImportInfo
    
    func body(content: Content) -> some View {
        let showAlert = Binding(
            get: { return true },
            set: { if !$0 {
                self.importInfo = .nothing
            } }
        )
        switch importInfo {
        case .nothing, .importedAll(items: _):
            return AnyView(content)
        case .importedNone( _, let authStatus), .importedSome(_, let authStatus):
            let onlySomeFailed: Bool = {
                if case .importedSome(_,_) = importInfo {
                    return true
                } else {
                    return false
                }
            }()
            let settingsOption = [PHAuthorizationStatus.denied, PHAuthorizationStatus.limited].contains(authStatus)
            let pickOption = [PHAuthorizationStatus.limited].contains(authStatus)
            return AnyView(content
                .alert(
                    "Error",
                    isPresented: showAlert
                ) {
                    if settingsOption, let url = URL(string: UIApplication.openSettingsURLString),
                       UIApplication.shared.canOpenURL(url) {
                        Button("Open settings", role: .none) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                    }
                    if pickOption, let vc = UIWindow.getTopViewController() {
                        Button("Select photos available to app", role: .none) {
                            showAlert.wrappedValue.toggle()
                            PHPhotoLibrary.shared().presentLimitedLibraryPicker(from: vc)
                        }
                    }
                    Button("Cancel", role: .cancel) {
                    }
                } message: {
                    Text("Failed to load " + (onlySomeFailed ? "some" : "any") + " of the selected items!" + (
                        pickOption ? " You need to give the app access to the selected photos." :
                            settingsOption ? " You need to give the app access to the photos library." : ""))
                }
            )
        }
    }
}

struct NavBarModifier: ViewModifier {
    func body(content: Content) -> some View {
        return content.navigationBarTitle("", displayMode: .inline)
    }
}

extension View {
    func withNavBar(contentView: ContentView) -> some View {
        modifier(NavBarModifier())
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let settings = Settings.shared
        settings.onboarded = true
        settings.isOn = true
        let cameraItems = CameraItems(cameraItems: [])
        return ContentView().environmentObject(settings).environmentObject(MediaItemCollection(mediaItems:[]))
            .environmentObject(cameraItems)
    }
}

struct PhotoPicker: UIViewControllerRepresentable {
    var parent: ContentView?
    @EnvironmentObject<CameraItems> var cameraItems: CameraItems
    @Environment(\.dismiss) var dismiss

    @Binding var importInfo: ImportInfo
    
    func makeCoordinator() -> PhotoPicker.Coordinator {
        return Coordinator(parent: self)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<PhotoPicker>) -> PHPickerViewController {
        self.importInfo = .nothing
        
        let photoLibrary = PHPhotoLibrary.shared()
        var configuration = PHPickerConfiguration(photoLibrary: photoLibrary)
        configuration.selectionLimit = 10
        configuration.filter = PHPickerFilter.any(of: [PHPickerFilter.images, PHPickerFilter.videos])
        let picker = PHPickerViewController(configuration: configuration)
        picker.delegate = context.coordinator
        return picker
    }
    
    func updateUIViewController(_ uiViewController: PHPickerViewController, context: Context) {
        
    }
    
    class Coordinator: NSObject, PHPickerViewControllerDelegate, UINavigationControllerDelegate {
        let parent:PhotoPicker
        
        init(parent:PhotoPicker) {
            self.parent = parent
        }
        
        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            let dispatchGroup = DispatchGroup()

            var index = 0
            var items: [CameraItem] = []

            for pickedItem in results {
                index += 1
                if let localAssetIdentifier = pickedItem.assetIdentifier {
                    dispatchGroup.enter()
                    let fetchResult = PHAsset.fetchAssets(withLocalIdentifiers: [localAssetIdentifier], options: nil)
                    if fetchResult.count > 0 {
                        DispatchQueue.main.async {
                            let asset = fetchResult.object(at: 0)
                            let item = CameraItem(asset: asset)
                            item.withData { data in
                                if let data = data {
                                    let fileName = PHAssetResource.assetResources(for: asset).first?.originalFilename ?? "imported"
                                    let fileExtension = PHAssetResource.assetResources(for: asset).first?.originalFilename.components(separatedBy: ".").last
                                    let (_, signedData) = C2PAHelper.shared.signData(data: data, filename: fileName, isCapture: false)
                                    let utType = UTType(PHAssetResource.assetResources(for: asset).first?.uniformTypeIdentifier ?? "image/heic")
                                    if let signedData = signedData,
                                        let mediaHash = Proof.shared.sha256(data: signedData),
                                       let path = Proof.shared.proofFilePath(mediaItem: item, hash: mediaHash, fileExtension: (fileExtension != nil) ? String(fileExtension!) : "JPG", fileSuffix: "_c2pa") {
                                        do {
                                            try signedData.write(to: path, options: [.atomic])
                                            let signedItem = CameraItem(mediaHash, mediaUrl: path, mediaType: utType, save: false, fileName: nil)
                                            items.append(signedItem)
                                        } catch {
                                        }
                                        dispatchGroup.leave()
                                    } else {
                                        dispatchGroup.leave()
                                    }
                                }
                            }
                        }
                        continue
                    } else {
                        dispatchGroup.leave()
                    }
                }
                
                // Try to fetch via URL instead
//                let itemProvider = pickedItem.itemProvider
//                for type in [UTType.data, UTType.image, UTType.video, UTType.livePhoto, UTType.movie] {
//                    if itemProvider.hasItemConformingToTypeIdentifier(type.identifier) {
//                        dispatchGroup.enter()
//                        itemProvider.loadItem(forTypeIdentifier: type.identifier, options: nil) {  [ index = index ] (url, error) in
//                            if let url = url as? URL {
//                                let item = CameraItem("\(millisecondsNow)_\(index)", mediaUrl: url, mediaType: type)
//                                items.append(item)
//                                dispatchGroup.leave()
//                            } else {
//                                dispatchGroup.leave()
//                            }
//                        }
//                        break
//                    }
//                }
            }
            
            dispatchGroup.notify(queue: DispatchQueue.global()) {
                DispatchQueue.main.async {
                    var importInfo: ImportInfo = .nothing
                    if results.count > 0 {
                        let authStatus = PHPhotoLibrary.authorizationStatus(for: .readWrite)
                        if items.count == results.count {
                            importInfo = .importedAll(items: items)
                        } else if items.count == 0 {
                            importInfo = .importedNone(items: items, authStatus: authStatus)
                        } else {
                            importInfo = .importedSome(items: items, authStatus: authStatus)
                        }
                    }

                    self.parent.importInfo = importInfo
                    self.parent.dismiss()
                }
            }
        }
    }
}
