//
//  Styles.swift
//  Proofmode
//
//  Created by N-Pex on 2020-07-17.
//

import SwiftUI

extension Font {
    static var titleFont:String {
        return "Hurme Geometric Sans 1"
    }
    
    static var bodyFont:String {
        return "Verdana"
    }
}

struct ScrollableStack: ViewModifier {
    func body(content: Content) -> some View {
        return
            GeometryReader { geometry in
                ScrollView(.vertical, showsIndicators: false) {
                    content
                }
            .frame(height: geometry.size.height, alignment: .top)
        }
    }
}

extension View {
    func scrollable() -> some View {
        self.modifier(ScrollableStack())
    }
}
