//
//  ShareSheetPopup.swift
//  Proofmode
//
//  Created by N-Pex on 2023-03-31.
//

import SwiftUI

struct ShareSheetPopup: View {
    @Environment(\.presentationMode) var presentationMode
    @Binding var itemToShare: ShareItemInfo
    @State var showingSystemShareSheet: Bool = false
    @State var uploadingToProofSign: Bool = false
    
    var body: some View {
        switch itemToShare {
        case .shareNothing:
            EmptyView()
            
        case .shareMedia(assets: let assets, url: let url):
            if showingSystemShareSheet {
                Text("").withDetents(fraction: 0.0) // Basically hide the "Bundling items" view
            } else if let url = url, url.exists {
                Text("").withDetents(fraction: 0.0).onAppear {
                    showSystemShareSheet(item: url, activityType: ActivityTypeShared(items: assets, fileName: url.lastPathComponent))
                }
            } else {
                BundleItemsView(onBundlingDone: { item, fileName in
                    showSystemShareSheet(item: item, activityType: ActivityTypeShared(items: assets, fileName: fileName), deleteFileOnCancel: true)
                }, onBundlingFailed: {
                    // TODO - Close popup?
                })
                .withDetents()
                .environment(\.selectedAssets, assets)
            }
        
        case .shareC2PASignedMedia(assets: let assets):
            if showingSystemShareSheet {
                Text("").withDetents(fraction: 0.0) // Basically hide the "Bundling items" view
            } else if uploadingToProofSign {
                C2PASignItemsView(assets: assets, onSigningDone: { urlSignedZip in
                    showSystemShareSheet(item: urlSignedZip, activityType: nil, deleteFileOnCancel: true)
                }, onSigningFailed: {
                }).withDetents()
            } else {
                BundleItemsView(onBundlingDone: { item, fileName in
                    self.uploadingToProofSign = true
                }, onBundlingFailed: {
                    // TODO - Close popup?
                })
                .withDetents()
                .environment(\.selectedAssets, assets)
            }
            
        case .sharePublicKey(let item):
            Text("").withDetents(fraction: 0.0).onAppear {
                showSystemShareSheet(item: item, activityType: ActivityTypePublicKeyShared(key: item))
            }
        }
    }
    
    func showSystemShareSheet(item: Any, activityType: ActivityType?, deleteFileOnCancel: Bool = false) {
        self.showingSystemShareSheet = true
        
        // Ideally we would just wrap a UIActivityViewController in UIViewControllerRepresentable and
        // show that in the view hierarchy above, but then, FOR SOME REASON - hint: Apple - the
        // completionWithItemsHandler will only be called if the small "x" is used to close the activity
        // view, not when it is closed with a swipe. So we would not log the share if a swipe was used...
        // Insread, get top view controller and present on that!
        let controller = UIActivityViewController(activityItems: [item], applicationActivities: nil)
        controller.excludedActivityTypes = nil
        controller.completionWithItemsHandler = { chosenActivityType, success, returnedItems, error in
            if success, let activityType = activityType {
                // Store the share activity to the log
                let _ = Activities.createActivity(type: activityType, startTime: Date.now)
            }
            else if !success, deleteFileOnCancel, let url = item as? URL, url.exists {
                // Delete the zip file, it was never used
                try? FileManager.default.removeItem(at: url)
            }
            self.presentationMode.wrappedValue.dismiss()
            self.itemToShare = .shareNothing
            self.showingSystemShareSheet = false
        }
        UIWindow.getTopViewController()?.present(controller, animated: true, completion: nil)
    }
}


fileprivate extension View {
    @ViewBuilder
    func withDetents(fraction: CGFloat = 0.4) -> some View {
        if #available(iOS 16.0, *) {
            self.presentationDetents([.fraction(fraction)])
        } else {
            self
        }
    }
}

public extension UIWindow {
    static func getTopViewController() -> UIViewController? {
        let allScenes = UIApplication.shared.connectedScenes
        let scene = allScenes.first { $0.activationState == .foregroundActive }
        if let windowScene = scene as? UIWindowScene {
            let keyWindow = windowScene.keyWindow
            if var topController = keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                return topController
            }
        }
        return nil
    }
}
