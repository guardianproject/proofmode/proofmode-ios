//
//  Date+Proofmode+Shared.swift
//  Proofmode
//
//  Created by N-Pex on 2023-03-23.
//

import UIKit

extension Date {
    
    /**
     Return a string representing current time in milliseconds since epoch
     */
    static func millisecondsNow() -> String {
        return "\(Int(Date().timeIntervalSince1970 * 1000))"
    }

    static func millisecondsNowAsInt() -> Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    func isToday() -> Bool {
        return Calendar.current.isDateInToday(self)
    }
    
    /**
     Return true if date is from the last 24-hour period
     */
    func isFromLatestDay() -> Bool {
        return self.timeIntervalSinceNow >= -(60 * 60 * 24)
    }
    
    func displayFormatted() -> String {
        if self.isToday() {
            return "Today \(self.formatted(.dateTime.hour().minute()))"
        } else if self.isFromLatestDay() {
            return "Yesterday \(self.formatted(.dateTime.hour().minute()))"
        }
        return self.formatted(.dateTime.day().month().year())
    }
    
    func metadataDateDate() -> String {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("MMM d, yyyy")
        return formatter.string(from: self)
    }

    func metadataDateTime() -> String {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("EEEE at h:mm a")
        return formatter.string(from: self)
    }
}
