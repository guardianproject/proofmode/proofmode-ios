//
//  EnvironmentValues+Proofmode+Shared.swift
//  Proofmode
//
//  Created by N-Pex on 2023-05-09.
//

import SwiftUI

public extension EnvironmentValues {
    static var gIsPreview: Bool {
#if DEBUG
        return ProcessInfo.processInfo.environment["XCODE_RUNNING_FOR_PREVIEWS"] == "1"
#else
        return false
#endif
    }
    
    var isPreview: Bool {
#if DEBUG
        return ProcessInfo.processInfo.environment["XCODE_RUNNING_FOR_PREVIEWS"] == "1"
#else
        return false
#endif
    }
}
