//
//  C2PASignItemsView.swift
//  Proofmode
//
//  Created by N-Pex on 2023-08-17.
//


import SwiftUI
import Photos
import LibProofMode
import AppTrackingTransparency
import ZIPFoundation
import Combine

struct C2PASignItemsView: View {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @State var assets: [CameraItem]
    
    @State var operationText: String = ""
    @State var statusText: String = ""
    @State var showProgressView: Bool = true
    @State var sendMedia: Bool = true
    var onSigningDone: ((Any) -> Void)
    var onSigningFailed: (() -> Void)
    @State var uploadTask: AnyCancellable? = nil
    
    var body: some View {
        VStack(alignment: .center, spacing: 15) {
            Text(operationText)
                .font(.custom(Font.titleFont, size: 21))
                .fontWeight(.bold)
                .foregroundColor(Color("colorDarkGray"))
                .multilineTextAlignment(.center)
                .padding()
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle())
                .opacity(showProgressView ? 1 : 0)
            Text(statusText)
                .font(.custom(Font.bodyFont, size: 15))
                .fontWeight(.bold)
                .foregroundColor(Color("colorDarkGray"))
                .multilineTextAlignment(.center)
                .padding()
        }.padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(trailing:
                                    HStack {
                Button(action: {
                    withAnimation {
                        self.mode.wrappedValue.dismiss()
                    }
                }) {
                    Image(systemName: "xmark")
                        .imageScale(.large)
                }
                
            }
            )
            .onAppear {
                sendToProofSign()
            }
    }
    
    func sendToProofSign() {
//        operationText = "Signing data..."
//        statusText = ""
//        showProgressView = true
//        
//        DispatchQueue.global(qos: .default).async {
//            if let item = ProofZipHelper.createProofZip(assets: self.assets, addMedia: sendMedia),
//               let data = try? Data(contentsOf: item.0), let signedData = try? signC2paZip(zipData: data) {
//                print("Got it", signedData.count)
//                DispatchQueue.main.async {
//                    statusText = "Verifying"
//                    
//                    // Save to hash directory
//                    if let archive = Archive(data: signedData, accessMode: .read) {
//                        for entry in archive {
//                            if entry.path.hasPrefix("c2pa-") {
//                                let index = entry.path.index(entry.path.startIndex, offsetBy: 5) // Strip the c2pa- part
//                                let entryFilename = String(entry.path[index...])
//                                if let mediaHash = item.1[entryFilename],
//                                   let mediaItem = self.assets.first(where: { asset in
//                                       mediaHash == (asset.mediaItemHash ?? "")
//                                   }),
//                                   let proofFolder = Proof.shared.proofFolder(for: mediaItem) {
//                                    let dest = proofFolder.appendingPathComponent(entry.path, conformingTo: UTType.jpeg)
//                                    let _ = try? archive.extract(entry, to: dest, skipCRC32: true)
//                                }
//                            }
//                        }
//                        
//                        // Try to save it
//                        let signedZipUrl = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(
//                            ProcessInfo.processInfo.globallyUniqueString).appendingPathExtension(".zip")
//                        do {
//                            try signedData.write(to: signedZipUrl)
//                            self.onSigningDone(signedZipUrl)
//                        } catch {
//                            self.onSigningDone(signedData)
//                        }
//                    }
//                    else {
//                        statusText = "There was a problem verifying or generating content credentials."
//                        self.showProgressView = false
//                        self.onSigningFailed()
//                    }
//                }
//            } else {
//                DispatchQueue.main.async {
//                    statusText = "There was a problem verifying or generating content credentials."
//                    self.showProgressView = false
//                    self.onSigningFailed()
//                }
//            }
//        }
    }
}

struct C2PASignItemsView_Previews: PreviewProvider {
    static var previews: some View {
        BundleItemsView(onBundlingDone: {_,_ in}, onBundlingFailed: {})
    }
}

public enum FileUploadError: Error {
    case failedToSignData
    case failedToSaveResponse
}

class C2PAUploader: NSObject {
    typealias Percentage = Double
    typealias Publisher = AnyPublisher<Percentage, Error>
    
    private typealias Subject = CurrentValueSubject<Percentage, Error>
    
    private lazy var urlSession = URLSession(
        configuration: .default,
        delegate: self,
        delegateQueue: .main
    )
    
    private var subjectsByTaskID = [Int : Subject]()
    
    func uploadFile(data: Data, at fromUrl: URL, with filename: String, mimeType: String,
                    to targetURL: URL, onSigningFileSaved: @escaping ((URL) -> Void)) -> Publisher {
        
        var request = URLRequest(
            url: targetURL,
            cachePolicy: .reloadIgnoringLocalCacheData
        )
        
        let boundary = "Boundary-\(UUID().uuidString)"
        
        request.httpMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.timeoutInterval = 120
        
        let httpBody = NSMutableData()
        httpBody.append(convertFileData(fieldName: "file",
                                        fileName: filename,
                                        mimeType: mimeType,
                                        fileData: data,
                                        using: boundary))
        httpBody.appendString("--\(boundary)--")
        
        let subject = Subject(0)
        var removeSubject: (() -> Void)?
        
        let task = urlSession.uploadTask(
            with: request,
            from: httpBody as Data,
            completionHandler: { data, response, error in
                if let data = data, error == nil {
                    let signedZipUrl = URL(string: fromUrl.deletingPathExtension().absoluteString.appending("-c2pa.zip"))
                    if let signedZipUrl = signedZipUrl {
                        do {
                            try data.write(to: signedZipUrl)
                            onSigningFileSaved(signedZipUrl)
                            subject.send(completion: .finished)
                        } catch {
                            subject.send(completion: .failure(FileUploadError.failedToSaveResponse))
                        }
                    } else {
                        subject.send(completion: .failure(FileUploadError.failedToSaveResponse))
                    }
                } else {
                    subject.send(completion: .failure(FileUploadError.failedToSignData))
                }
                // Validate response and send completion
                removeSubject?()
            }
        )
        
        subjectsByTaskID[task.taskIdentifier] = subject
        removeSubject = { [weak self] in
            self?.subjectsByTaskID.removeValue(forKey: task.taskIdentifier)
        }
        
        task.resume()
        
        return subject.eraseToAnyPublisher()
    }
    
    func convertFileData(fieldName: String, fileName: String, mimeType: String, fileData: Data, using boundary: String) -> Data {
        let data = NSMutableData()
        
        data.appendString("--\(boundary)\r\n")
        data.appendString("Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(fileName)\"\r\n")
        data.appendString("Content-Type: \(mimeType)\r\n\r\n")
        data.append(fileData)
        data.appendString("\r\n")
        
        return data as Data
    }
}

fileprivate extension NSMutableData {
    func appendString(_ string: String) {
        if let data = string.data(using: .utf8) {
            self.append(data)
        }
    }
}

extension C2PAUploader: URLSessionTaskDelegate {
    func urlSession(
        _ session: URLSession,
        task: URLSessionTask,
        didSendBodyData bytesSent: Int64,
        totalBytesSent: Int64,
        totalBytesExpectedToSend: Int64
    ) {
        let progress = Double(totalBytesSent) / Double(totalBytesExpectedToSend)
        let subject = subjectsByTaskID[task.taskIdentifier]
        subject?.send(progress)
    }
}
