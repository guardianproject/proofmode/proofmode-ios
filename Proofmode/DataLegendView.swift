//
//  DataLegendView.swift
//  Proofmode
//
//  Created by N-Pex on 2020-07-17.
//

import SwiftUI

struct DataLegendView: View {
    var body: some View {
        VStack {
            WebView(url: Bundle.main.url(forResource: "datalegend", withExtension: "html"))
        }
        .navigationBarTitle("Data Legend", displayMode: .inline)
    }
}

struct DataLegendView_Previews: PreviewProvider {
    static var previews: some View {
        DataLegendView()
    }
}
