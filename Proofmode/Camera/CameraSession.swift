//
//  CameraSession.swift
//  Proofmode
//
//  Created by N-Pex on 2024-01-24.
//

import SwiftUI
import CameraManager
import Photos
import LegacyUTType

class CameraSession: ObservableObject {
    @Published var itemsBeingProcessed: Bool = false
    
    private var processingItems: [Int64: CameraItem] = [:]
    private var cancellables = [AnyObject]()
    private var whenSavedCallback: (([CameraItem]) -> Void)? = nil
    
    init() {
    }
    
    public var count: Int {
        get {
            return processingItems.count
        }
    }
    
    public var cameraItemsOrdered: [CameraItem] {
        get {
            return self.processingItems.values.sorted(by: { left, right in
                return (left.creationDate?.timeIntervalSince1970 ?? 0) < (right.creationDate?.timeIntervalSince1970 ?? 0)
            })
        }
    }
    
    public func addItem(_ id: Int64, type: UTType) {
        let ci = CameraItem(nil, mediaData: Data(), mediaType: type, save: false)
        ci.modifiedDate = Date(milliseconds: id)
        ci.state = .saving
        ci.fileName =  "\(id).\(type == UTType.video ? "mp4" : "jpg")"
        self.processingItems.updateValue(ci, forKey: id)
        self.updateItemsBeingProcessed()
        
        let c = ci.objectWillChange.sink(receiveValue: {
            self.updateItemsBeingProcessed()
        })
        
        // Important: You have to keep the returned value allocated,
        // otherwise the sink subscription gets cancelled
        self.cancellables.append(c)
        self.update()
    }
    
    public func setItemData(id: Int64, captureResult: CaptureResult) {
        guard let cameraItem = self.processingItems[id] else { return }
        if case .success(let content) = captureResult, let data = content.asData {
            cameraItem.data = data
            DispatchQueue.main.async {
                cameraItem.objectWillChange.send()
            }
            
            // Sign and save
            DispatchQueue.global(qos: .utility).async {
                let (signedUrl, signedData) = Settings.shared.optionCredentials ? C2PAHelper.shared.signData(data: data, filename: cameraItem.filenameOrDefault, isCapture: true) : (nil, data)
                cameraItem.mediaUrl = signedUrl
                cameraItem.data = signedData
                cameraItem.save { _ in
                    DispatchQueue.main.async {
                        if let signedUrl = signedUrl {
                            try? FileManager.default.removeItem(at: signedUrl)
                        }
                        self.updateItemsBeingProcessed()
                        cameraItem.objectWillChange.send()
                    }
                }
            }
        }
        else {
            DispatchQueue.main.async {
                self.processingItems.removeValue(forKey: id)
                self.updateItemsBeingProcessed()
            }
        }
    }
    
    public func setItemUrl(id: Int64, url: URL) {
        guard let cameraItem = self.processingItems[id] else { return }
        DispatchQueue.global(qos: .userInitiated).async {
            let signedUrl = Settings.shared.optionCredentials ? C2PAHelper.shared.signUrl(url: url, filename: cameraItem.filenameOrDefault, isCapture: true) : url
            cameraItem.mediaUrl = signedUrl
            cameraItem.save { _ in
                DispatchQueue.main.async {
                    try? FileManager.default.removeItem(at: url)
                    try? FileManager.default.removeItem(at: signedUrl)
                    self.updateItemsBeingProcessed()
                    cameraItem.objectWillChange.send()
                }
            }
        }
    }
    
    public func update() {
        DispatchQueue.main.async {
            self.objectWillChange.send()
        }
    }
    
    private func updateItemsBeingProcessed() {
        self.itemsBeingProcessed = processingItems.values.contains(where: { ci in
            switch ci.state {
            case .initial:
                return true
            case .saving:
                return true
            default: return false
            }
        })
        if !self.itemsBeingProcessed, let callback = self.whenSavedCallback {
            self.whenSavedCallback = nil
            DispatchQueue.main.async {
                callback(self.cameraItemsOrdered)
            }
        }
    }
    
    /***
     Wait until all items have been signed and saved, then call the given block! */
    public func whenSaved(then: @escaping ([CameraItem]) -> Void) {
        self.whenSavedCallback = then
        self.updateItemsBeingProcessed()
    }
}

fileprivate extension Date {
    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}
