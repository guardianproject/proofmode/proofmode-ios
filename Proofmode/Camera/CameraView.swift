//
//  CameraView.swift
//  Proofmode
//
//  Created by N-Pex on 2023-01-24.
//

import SwiftUI
import CameraManager
import Photos
import LibProofMode

open class OrientationFixedCameraManager: CameraManager {
    override open func resetOrientation() {
        super.resetOrientation()
    }
}

struct CameraView: View {
    @ObservedObject var cameraManager: CameraManagerObserver
    @Binding var showCamera: Bool
    var onDone: (() -> Void)
    
    @State var cameraAuthStatus: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
    
    var body: some View {
        ZStack(alignment: .top) {
            if cameraAuthStatus == .authorized {
                CameraManagerView(cameraManager: cameraManager, showCamera: $showCamera)
                CameraOverlayView(cameraManager: cameraManager)
            } else {
                GeometryReader { geo in
                    ZStack(alignment: .center) {
                        VStack(alignment: .center, spacing: 6) {
                            Text("You don't have permission to access the camera.").foregroundColor(.white)
                            Button(cameraAuthStatus == .notDetermined ? "Allow" : "Open settings") {
                                if cameraAuthStatus == .notDetermined {
                                    AVCaptureDevice.requestAccess(for: .video) { access in
                                        self.cameraAuthStatus = AVCaptureDevice.authorizationStatus(for: .video)
                                    }
                                } else {
                                    // Open privacy settings
                                    guard let url = URL(string: UIApplication.openSettingsURLString),
                                          UIApplication.shared.canOpenURL(url) else {
                                        assertionFailure("Not able to open App privacy settings")
                                        return
                                    }
                                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                }
                            }.tint(.gray)
                        }
                    }
                    .frame(width: geo.size.width, height: geo.size.height)
                    .background(.black)
                }
            }
            VStack {
                HStack(alignment: .center) {
                    Spacer()
                    if cameraManager.isRecording {
                        Text(cameraManager.recordedDisplayTime ?? "").foregroundColor(.white)
                            .padding(20)
                        Spacer()
                    } else {
                        Button(action: {
                            onDone()
                        }) {
                            Text("Done")
                        }.padding(20)
                    }
                }.background(Color.black.opacity(0.2)).tint(Color.white)
                if cameraAuthStatus == .authorized {
                    CameraLibraryWarningView()
                }
            }
        }
        .onAppear {
            cameraManager.cameraManager.shouldUseLocationServices = Settings.shared.optionLocation
            if cameraAuthStatus == .notDetermined {
                AVCaptureDevice.requestAccess(for: .video) { authed in
                    self.cameraAuthStatus = AVCaptureDevice.authorizationStatus(for: .video)
                }
            }
        }
    }
}

struct CameraView_Previews: PreviewProvider {
    @State static var showCamera: Bool = false
    static var previews: some View {
        CameraView(
            cameraManager: CameraManagerObserver(cameraManager: OrientationFixedCameraManager()),
            showCamera: $showCamera
        )
        {
        }
    }
}

/**
 CameraManager does not handle device orientation changes correctly, in that it resizes itself to the
 parent bounds, but it does so BEFORE the parent is updated. So we subclass UIViewController and
 listen to changes ourselves.
 */
fileprivate class CameraViewController: UIViewController {
    var cameraManager: CameraManager
    
    init( cameraManager : CameraManager ) {
        self.cameraManager = cameraManager
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var observer: NSKeyValueObservation?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(orientationChange), name: UIDevice.orientationDidChangeNotification, object: nil)
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIDevice.current.endGeneratingDeviceOrientationNotifications()
    }
    
    @objc func orientationChange() -> Void {
        DispatchQueue.main.async {
            self.cameraManager.deviceOrientation = UIDevice.current.orientation
            self.cameraManager.resetOrientation()
        }
    }
}

struct CameraManagerView: UIViewControllerRepresentable {
    @ObservedObject var cameraManager: CameraManagerObserver
    @Binding var showCamera: Bool
    
    func makeCoordinator() -> CameraManagerView.Coordinator {
        return Coordinator()
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<CameraManagerView>) -> UIViewController {
        let uiViewController = CameraViewController(cameraManager: cameraManager.cameraManager)
        return uiViewController
    }
    
    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
        if showCamera {
            if !context.coordinator.installed {
                context.coordinator.installed = true
                cameraManager.cameraManager.addLayerPreviewToView(uiViewController.view, newCameraOutputMode: cameraManager.cameraManager.cameraOutputMode, completion: {
                    cameraManager.cameraManager.resumeCaptureSession()
                })
            }
        } else if context.coordinator.installed {
            context.coordinator.installed = false
            cameraManager.cameraManager.stopCaptureSession()
        }
    }
    
    class Coordinator: NSObject, UINavigationControllerDelegate {
        var installed: Bool = false
    }
}

class CameraManagerObserver: ObservableObject {
    var timer: Timer? = nil
    
    @Published var cameraManager: OrientationFixedCameraManager
    @Published var isRecording: Bool = false {
        didSet {
            if self.isRecording, timer == nil {
                timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { timer in
                    self.recordedDisplayTime = self.cameraManager.recordedDuration.positionalTime
                })
            } else if !self.isRecording, let timer = timer {
                timer.invalidate()
                self.timer = nil
                self.recordedDisplayTime = nil
            }
        }
    }
    @Published var recordedDisplayTime: String? = nil
    @Published var currentSession: CameraSession = CameraSession()
    
    init(cameraManager: OrientationFixedCameraManager) {
        self.cameraManager = cameraManager
        cameraManager.showErrorsToUsers = false
        cameraManager.writeFilesToPhoneLibrary = false
        cameraManager.shouldRespondToOrientationChanges = false
        cameraManager.shouldKeepViewAtOrientationChanges = true
    }
    
    public func update() {
        DispatchQueue.main.async {
            self.objectWillChange.send()
        }
    }
    
    public func startNewCaptureSession() {
        self.currentSession = CameraSession()
    }
}

struct CameraLibraryWarningView: View {
    
    @State var authState: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus(for: .readWrite)
    
    /**
     If we were dismissed by the user
     */
    @State var silenced: Bool = false
    
    var body: some View {
        GeometryReader { geoTop in
            if authState == .authorized || authState == .limited || self.silenced {
                ZStack {}
            } else {
                HStack(alignment: .top, spacing: 10) {
                    VStack(alignment: .leading, spacing: 6) {
                        Text("You don't have permission to write to photo library, captured items will not be saved.").foregroundColor(.white)
                        Button(authState == .notDetermined ? "Allow" : "Open settings") {
                            if authState == .notDetermined {
                                PHPhotoLibrary.requestAuthorization(for: .readWrite) { status in
                                    self.authState = status
                                }
                            } else {
                                // Open privacy settings
                                guard let url = URL(string: UIApplication.openSettingsURLString),
                                      UIApplication.shared.canOpenURL(url) else {
                                    assertionFailure("Not able to open App privacy settings")
                                    return
                                }
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            }
                        }.tint(.gray)
                    }
                    Button(action: {
                        self.silenced = true
                    }) {
                        Image(systemName: "xmark.circle")
                        
                    }.tint(.white)
                }
                .padding(16)
                .frame(width: geoTop.size.width)
                .background(.black.opacity(0.2))
                .overlay(
                    RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.white, lineWidth: 1)
                        .padding(8)
                )
            }
        }
    }
}

struct CameraLibraryWarningView_Previews: PreviewProvider {
    static var previews: some View {
        CameraLibraryWarningView()
    }
}


struct CameraOverlayView: View {
    @Environment(\.isPreview) var isPreview
    
    @ObservedObject var cameraManager: CameraManagerObserver
    
    var body: some View {
        VStack {
            Spacer().allowsHitTesting(false)
            HStack(alignment: .center) {
                if self.cameraManager.cameraManager.cameraOutputMode == .stillImage {
                    Button(action: {
                        self.cameraManager.cameraManager.cameraOutputMode = .videoWithMic
                        self.cameraManager.update()
                    }) {
                        Image("ic_cam_video")
                            .resizable()
                            .frame(width: 30, height: 30)
                            .tint(.white)
                    }.padding(20)
                    
                } else {
                    Button(action: {
                        self.cameraManager.cameraManager.cameraOutputMode = .stillImage
                        self.cameraManager.update()
                    }) {
                        Image("ic_cam_photo")
                            .resizable()
                            .frame(width: 30, height: 30)
                            .tint(.white)
                    }.padding(20).disabled(self.cameraManager.isRecording)
                }
                Spacer()
                if self.cameraManager.cameraManager.cameraOutputMode == .videoWithMic || self.cameraManager.cameraManager.cameraOutputMode == .videoOnly {
                    if self.cameraManager.isRecording {
                        Button(action: {
                            let uniqueImageId = Date.millisecondsNowAsInt()
                            self.cameraManager.currentSession.addItem(uniqueImageId, type: UTType.video)
                            AudioServicesPlaySystemSound(1114)
                            self.cameraManager.cameraManager.stopVideoRecording { videoURL, error in
                                if let url = videoURL, error == nil {
                                    self.cameraManager.currentSession.setItemUrl(id: uniqueImageId, url: url)
                                }
                            }
                            self.cameraManager.isRecording = false
                            self.cameraManager.update()
                        }) {
                            ZStack(alignment: .center) {
                                RoundedRectangle(cornerRadius: 30)
                                    .stroke(Color.white, lineWidth: 2)
                                    .frame(width: 60, height: 60)
                                RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color.clear, lineWidth: 0)
                                    .background(.red).cornerRadius(4)
                                    .frame(width: 28, height: 28)
                            }
                        }
                        
                    } else {
                        Button(action: {
                            self.cameraManager.cameraManager.startRecordingVideo()
                            self.cameraManager.isRecording = true
                            self.cameraManager.update()
                            AudioServicesPlaySystemSound(1113)
                        }) {
                            ZStack(alignment: .center) {
                                RoundedRectangle(cornerRadius: 30)
                                    .stroke(Color.white, lineWidth: 2)
                                    .frame(width: 60, height: 60)
                                RoundedRectangle(cornerRadius: 24)
                                    .stroke(Color.clear, lineWidth: 0)
                                    .background(.red).cornerRadius(24)
                                    .frame(width: 48, height: 48)
                            }
                        }.frame(maxWidth: .infinity)
                    }
                } else {
                    Button(action: {
                        let uniqueImageId = Date.millisecondsNowAsInt()
                        self.cameraManager.currentSession.addItem(uniqueImageId, type: UTType.image)
                        self.cameraManager.cameraManager.capturePictureWithCompletion { (result:CaptureResult) in
                            self.cameraManager.currentSession.setItemData(id: uniqueImageId, captureResult: result)
                        }
                    }) {
                        ZStack(alignment: .center) {
                            RoundedRectangle(cornerRadius: 30)
                                .stroke(Color.white, lineWidth: 2)
                                .frame(width: 60, height: 60)
                            RoundedRectangle(cornerRadius: 20)
                                .stroke(Color.clear, lineWidth: 0)
                                .background(.white).cornerRadius(20)
                                .frame(width: 40, height: 40)
                        }
                    }
                    .frame(maxWidth: .infinity)
                }
                Spacer()
                Button(action: {
                    self.cameraManager.cameraManager.cameraDevice = self.cameraManager.cameraManager.cameraDevice == .front ? .back : .front
                    self.cameraManager.update()
                }) {
                    Image("ic_switch")
                        .resizable()
                        .frame(width: 30, height: 30)
                        .tint(.white)
                }.padding(20).disabled(self.cameraManager.isRecording)
            }
            .frame(width: nil, height: 100)
            .background(Color.black.opacity(0.2))
            .edgesIgnoringSafeArea([])
        }
        .edgesIgnoringSafeArea([])
    }
}

// From https://stackoverflow.com/questions/54662047/converting-cmtime-to-string-is-wrong-value-return
fileprivate extension CMTime {
    var roundedSeconds: TimeInterval {
        return seconds.rounded()
    }
    var hours:  Int { return Int(roundedSeconds / 3600) }
    var minute: Int { return Int(roundedSeconds.truncatingRemainder(dividingBy: 3600) / 60) }
    var second: Int { return Int(roundedSeconds.truncatingRemainder(dividingBy: 60)) }
    var positionalTime: String {
        return hours > 0 ?
        String(format: "%d:%02d:%02d",
               hours, minute, second) :
        String(format: "%02d:%02d",
               minute, second)
    }
}
