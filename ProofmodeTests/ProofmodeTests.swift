//
//  ProofmodeTests.swift
//  ProofmodeTests
//
//  Created by N-Pex on 2023-02-07.
//

import XCTest
import LibProofMode
import UniformTypeIdentifiers
import ObjectivePGP

final class ProofmodeTests: XCTestCase {
    var mediaItem: MediaItem?
    
    // Note - the expected hash below is calculated using another tool, BUT don't expect you can just
    // do "Show in finder" on the image and sha it, you need to do it on the image that gets BUILT
    // by xcode, i.e. the one that ends up in the bundle, cause XCode seems to do something with it...
    let testImageChecksum = "2bbb45932bde56e7742ddae032faf22704ab11835084f8ac9abca19d00737ab2"
    
    override func setUpWithError() throws {
        let path = Bundle(for: ProofmodeTests.self).path(forResource: "logo_1024", ofType: "png")
        XCTAssertNotNil(path, "Failed to find test image path")
        let url = URL(fileURLWithPath: path!)
        XCTAssertNotNil(url, "Failed to get url of test image")
        let data = try Data(contentsOf: url)
        XCTAssertNotNil(data, "Failed to read data of test image")
        mediaItem = MediaItem(mediaData: data, mediaType: UTType.image, modifiedDate: nil)
        XCTAssertNotNil(self.mediaItem, "Failed to setup test media item")
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testProofChecksum() throws {
        mediaItem!.forceGenerateProof = true
        let options = ProofGenerationOptions(showDeviceIds: false, showLocation: false, showMobileNetwork: false, notarizationProviders: [])
                
        let expectProofIsGenerated = XCTestExpectation(description: "Proof is generated")
        
        Proof.shared.process(mediaItem: mediaItem!, options: options) { item in
            expectProofIsGenerated.fulfill()
        }
        
        wait(for: [expectProofIsGenerated], timeout: 10)
        XCTAssertEqual(mediaItem!.mediaItemHash, testImageChecksum, "Wrong hash value calculated!")
    }

    func testCustomKeys() throws {
        let key = KeyGenerator().generate(for: "tests@proofmode.witness.org", passphrase: "soopersecret")
        let kr = Keyring()
        kr.import(keys: [key])
        
        do {
            let privateKey = try Armor.armored(key.export(),as: .secretKey)
            Proof.shared.initializeWithKeys(privateKey: privateKey, passphrase: "soopersecret")
            
            mediaItem!.forceGenerateProof = true
            let options = ProofGenerationOptions(showDeviceIds: false, showLocation: false, showMobileNetwork: false, notarizationProviders: [])
                    
            let expectProofIsGenerated = XCTestExpectation(description: "Proof is generated")
            
            Proof.shared.process(mediaItem: mediaItem!, options: options) { item in
                expectProofIsGenerated.fulfill()
            }
            
            wait(for: [expectProofIsGenerated], timeout: 10)
            XCTAssertEqual(mediaItem!.mediaItemHash, testImageChecksum, "Wrong hash value calculated!")

        } catch {
            XCTFail("Failed to generate test keys")
        }
    }
    
    func testProofFiles() throws {
        mediaItem!.forceGenerateProof = true
        
        Proof.shared.initializeWithDefaultKeys()
        
        let options = ProofGenerationOptions(showDeviceIds: false, showLocation: false, showMobileNetwork: false, notarizationProviders: [])
                
        let expectProofIsGenerated = XCTestExpectation(description: "Proof is generated")
        
        Proof.shared.process(mediaItem: mediaItem!, options: options) { item in
            expectProofIsGenerated.fulfill()
        }
        
        wait(for: [expectProofIsGenerated], timeout: 10)
        XCTAssertNotNil(mediaItem!.mediaItemHash, "Failed to generate proof")
        let folder = Proof.shared.proofFolder(for: mediaItem!)
        XCTAssertNotNil(folder, "Media folder not found")
        
        let files = try FileManager.default.contentsOfDirectory(at: folder!, includingPropertiesForKeys: [])
        XCTAssertTrue(files.contains(where: { $0.lastPathComponent == "\(mediaItem!.mediaItemHash!).asc"}), "Failed to find generated .asc file")
        XCTAssertTrue(files.contains(where: { $0.lastPathComponent == "\(mediaItem!.mediaItemHash!).proof.json"}), "Failed to find generated .json file")
        XCTAssertTrue(files.contains(where: { $0.lastPathComponent == "\(mediaItem!.mediaItemHash!).proof.json.asc"}), "Failed to find generated .json.asc file")
        XCTAssertTrue(files.contains(where: { $0.lastPathComponent == "\(mediaItem!.mediaItemHash!).proof.csv"}), "Failed to find generated .csv file")
        XCTAssertTrue(files.contains(where: { $0.lastPathComponent == "\(mediaItem!.mediaItemHash!).proof.csv.asc"}), "Failed to find generated .csv.asc file")
    }

    
//    func testPerformanceExample() throws {
//        // This is an example of a performance test case.
//        measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

}
