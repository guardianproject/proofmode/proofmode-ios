//
//  ShareViewController.swift
//  ShareExtension
//
//  Created by N-Pex on 2023-01-12.
//

import UIKit
import SwiftUI
import UniformTypeIdentifiers
import Photos
import LibProofMode

class ShareViewController: UIViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let proof = Proof.shared
        proof.defaultDocumentFolder = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.org.witness.proofmode.ios")
        ?? FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

        let model = ShareExtensionViewModel()
        model.statusOperation = "Initializing..."
        model.statusText = ""

        let shareExtensionView = ShareExtensionView(vm: model)
        let childView = UIHostingController(rootView: shareExtensionView)
        self.addChild(childView)
        childView.view.frame = self.view.bounds
        self.view.addSubview(childView.view)
        childView.didMove(toParent: self)
        
        let dispatchGroup = DispatchGroup()
                
        var items: [CameraItem] = []
        var index = 0
        for item in (extensionContext?.inputItems as? [NSExtensionItem] ?? []) {
            for itemProvider in item.attachments ?? [] {
                for type in [UTType.data, UTType.image, UTType.video, UTType.livePhoto, UTType.movie] {
                    if itemProvider.hasItemConformingToTypeIdentifier(type.identifier) {
                        index += 1
                        dispatchGroup.enter()
                        itemProvider.loadItem(forTypeIdentifier: type.identifier, options: nil) { (url, error) in
                            if let shareURL = url as? URL {
                                let fileName = shareURL.lastPathComponent
                                let fileExtension = fileName.components(separatedBy: ".").last
                                let signedUrl = C2PAHelper.shared.signUrl(url: shareURL, filename: fileName, isCapture: false)
                                if 
                                    let signedData = try? Data(contentsOf: signedUrl),
                                    let mediaHash = Proof.shared.sha256(data: signedData),
                                    let emptyUrl = URL(string: "about:empty"),
                                    let path = Proof.shared.proofFilePath(mediaItem: MediaItem(mediaUrl: emptyUrl), hash: mediaHash, fileExtension: fileExtension ?? "JPG", fileSuffix: "_c2pa") {
                                    do {
                                        try signedData.write(to: path, options: [.atomic])
                                        let signedItem = CameraItem(mediaHash, mediaUrl: path, mediaType: type, save: false, fileName: fileName)
                                        items.append(signedItem)
                                    } catch {
                                    }
                                }
                                try? FileManager.default.removeItem(at: signedUrl)
                            }
                            dispatchGroup.leave()
                        }
                        break
                    }
                }
            }
        }

        dispatchGroup.notify(queue: DispatchQueue.global()) {
            DispatchQueue.main.async {
                
                let itemCount = items.count
                var currentItemIndex = 0
                
                model.statusOperation = "Generating proof..."
                model.statusText = "Processed \(currentItemIndex) of \(itemCount) items"

                let options = Settings.shared.toProofGenerationOptions()
                
                for item in items {
                    // Don't force proof generation!
                    item.forceGenerateProof = false
                    
                    currentItemIndex += 1
                    Proof.shared.whenDone { [index = currentItemIndex] in
                        model.statusText = "Processing item \(index) of \(itemCount)"
                    }
                    Proof.shared.process(mediaItem: item, options: options)
                }
                Proof.shared.whenDone {
                    model.statusOperation = "Proof generated"
                    model.statusText = ""
                    model.showSpinner = false

                    // Save to activity log
                    let _ = Activities.createActivity(type: ActivityTypeImported(items: items), startTime: Date.now)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: DispatchWorkItem(block: {
                        self.extensionContext?.completeRequest(returningItems: nil, completionHandler: nil)
                    }))
                }
            }
        }
    }
}

class ShareExtensionViewModel: ObservableObject {
    @Published var showSpinner = true
    @Published var statusOperation = "Generating proof..."
    @Published var statusText = ""
}

struct ShareExtensionView: View {
    @ObservedObject var vm: ShareExtensionViewModel

    var body: some View {
        VStack {
            if vm.showSpinner {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle())
                    .padding(EdgeInsets(top: 0, leading: 0, bottom: 20, trailing: 0))
                    .frame(width: 60,height: 60)
                    .scaleEffect(3)
            }
            Text(vm.statusOperation)
                .font(Font.custom(Font.titleFont, size: 20).smallCaps())
                .fontWeight(.bold)
                .foregroundColor(.black)
            Text(vm.statusText)
                .font(Font.custom(Font.titleFont, size: 13))
                .fontWeight(.regular)
                .multilineTextAlignment(.center)
                .foregroundColor(.black)
        }
    }
}

struct ShareExtension_Previews: PreviewProvider {
    static var vm = ShareExtensionViewModel()
    static var previews: some View {
        ShareExtensionView(vm: vm)
    }
}

extension Settings {
    static var openTimestampsNotarizationProvider: OpenTimestampsNotarizationProvider?
    func toProofGenerationOptions() -> ProofGenerationOptions {
        if Settings.openTimestampsNotarizationProvider == nil {
            Settings.openTimestampsNotarizationProvider = OpenTimestampsNotarizationProvider()
        }
        let options = ProofGenerationOptions(showDeviceIds: self.optionPhone, showLocation: self.optionLocation, showMobileNetwork: self.optionNetwork, notarizationProviders: [
            Settings.openTimestampsNotarizationProvider ?? OpenTimestampsNotarizationProvider()
        ])
        return options
    }
}
